﻿#pragma strict

public var effectRing : GameObject;

private var phaseNum = 0;
public var health = 0f;
private var speed = 1f;
public var totalHealth = 1f;

private var currAtkTime = 0;

private var wobbleH = false;
private var wobbleV = false;
private var hMin = -0.36f;
private var hMax = 4.33f;
private var hSpeed = 1f;
private var hLerp = 0f;
private var vMin = -0.01f;
private var vMax = 0.01f;
private var vSpeed = 1f;
private var vLerp = 1f;

private var triggerFlag = true;
private var isInvincible = true;

private var dTime=0f;
private var currSecond=0;

private var healthText:GameObject;
healthText = GameObject.Find("bossHealth");

//Mike's health bar here:


private var playerObj : GameObject; 
playerObj = GameObject.Find("spaceship");
private var scoreText : GameObject;
scoreText = GameObject.Find("ScoreText");
private var bossImgs : Sprite[];

//public var complexBG : GameObject;

function Start () {
	bossImgs = Resources.LoadAll.<Sprite>("bossShips");
	health = 250f;
	//healthText = GameObject.Find("bossHealth");
	//Debug.Log(healthText.GetComponent(UI.Text).text);//.GetComponent.<UnityEngine.UI.Text>());
	healthText.GetComponent(UI.Text).text = "Boss Health : "+health;
	
	//Mike's health bar here:
	totalHealth = health;
	//var totalHealth = health;
	
	
	currAtkTime = Time.time;
	toAngle(270);
	toggleWobble(0,true);
}

function Update () {

	dTime += (Time.deltaTime * 1);
	if(dTime>currSecond){currSecond++;Debug.Log("Boss currSecond="+currSecond);}
	
	//Debug.Log(health);
	
	if(wobbleH){
		hLerp += (Time.deltaTime*hSpeed);
		transform.position.x = Mathf.SmoothStep(hMin,hMax, Mathf.PingPong(hLerp, 1.0f));
		//Debug.Log(transform.position.x);
	}
	if(wobbleV){
		vLerp += (Time.deltaTime*vSpeed);
		transform.position.y += Mathf.SmoothStep(vMin,vMax, Mathf.PingPong(vLerp, 1.0f));
	}
	
	switch(phaseNum){
		
		case 0:
			
			if(transform.position.x >= GameObject.Find("playCenter").transform.position.x && transform.position.y<1){
				GetComponent.<Rigidbody2D>().velocity *= 0;
				toggleWobble(0,false); toggleWobble(1,true);
				transform.position.x = GameObject.Find("playCenter").transform.position.x;
				var ring = Instantiate(effectRing,transform.position,Quaternion.identity);
				ring.GetComponent("lerpScale").SendMessage("setMode","grow");
				phaseNum = 1;
				currAtkTime = Time.time;
				dTime=0;
				currSecond=0;
			}
			
			break;
		
		case 1:
			
			if(currSecond==1 && triggerFlag){
				triggerFlag = false;
				
			}
			else if(currSecond==2 && !triggerFlag){
				triggerFlag = true;
				concentrate();
			}
			else if(currSecond==4 && triggerFlag){
				triggerFlag = false;
				isInvincible=false;
				InvokeRepeating("B1_P1",0f,0.5f);
			}
			else if(currSecond==10 && !triggerFlag){
				triggerFlag = true;
				//InvokeRepeating("B1_P1",0f,0.5f);
				hSpeed = 0.25f; hLerp=0.5f;
				toggleWobble(0,true);
			}
			
			break;
			
		case 2:
			
			if(transform.position.x > (GameObject.Find("playCenter").transform.position.x-0.1f) && transform.position.x < (GameObject.Find("playCenter").transform.position.x+0.1f)){
				GetComponent.<Rigidbody2D>().velocity *= 0;
				toggleWobble(0,false); //toggleWobble(1);
				transform.position.x = GameObject.Find("playCenter").transform.position.x;
				var ring2 = Instantiate(effectRing,transform.position,Quaternion.identity);
				ring2.GetComponent("lerpScale").SendMessage("setMode","grow");
				GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("dropHyperPowUp",transform.position);
				phaseNum = 3;
				currAtkTime = Time.time;
				dTime=0;
				currSecond=0;
				triggerFlag=true;
			}
			
			break;
			
		case 3:
			
			if(currSecond==2 && triggerFlag){
				triggerFlag = false;
				concentrate();
			}
			else if(currSecond==4 && !triggerFlag){
				triggerFlag = true;
				isInvincible=false;
				InvokeRepeating("B1_P2",1f,0.45f);
				hSpeed = 0.5f; hLerp = 0.5f;
				toggleWobble(0,true);
				vSpeed = 1f;
			}
			else if(currSecond==6 && triggerFlag){
				triggerFlag = false;
				hSpeed += 0.5f;
				//InvokeRepeating("B1_P1",0f,0.5f);
				
			}
			else if(currSecond==8 && !triggerFlag){
				triggerFlag = true;
				hSpeed += 0.5f;
				//InvokeRepeating("B1_P1",0f,0.5f);
				
			}
			else if(currSecond==10 && triggerFlag){
				triggerFlag = false;
				hSpeed += 0.5f;
				//InvokeRepeating("B1_P1",0f,0.5f);
				
			}
			
			break;
			
		case 4:
			
			if(currSecond==1 && triggerFlag && playerObj.GetComponent(spaceshipScript).usedBomb){
				Debug.Log("boss died easy");
				health=0;
				toggleWobble(0,false); toggleWobble(1,false);
				elaborateDeath();
				triggerFlag=false;
			}
			if(triggerFlag && transform.position.x > (GameObject.Find("playCenter").transform.position.x-0.1f) && transform.position.x < (GameObject.Find("playCenter").transform.position.x+0.1f)){
				Debug.Log("boss dies hard");
				
				GameObject.Find("bossAngry").GetComponent.<AudioSource>().Play();
				GameObject.Find("BGM").GetComponent.<AudioSource>().Stop();
				GameObject.Find("bossAngryBGM").GetComponent.<AudioSource>().Play();
				
				GetComponent.<Rigidbody2D>().velocity *= 0;
				//toggleWobble(0); //toggleWobble(1);
				transform.position.x = GameObject.Find("playCenter").transform.position.x;
				var ring3 = Instantiate(effectRing,transform.position,Quaternion.identity);
				ring3.GetComponent("lerpScale").SendMessage("setMode","grow");
				
				//GameObject.FindWithTag("GameController").GetComponent("levelOneScript").SendMessage("dropHyperPowUp",transform.position);
				hMin = transform.position.x - 0.1;
				hMax = transform.position.x + 0.1;
				hSpeed = 50f;
				hLerp = 0f;
				GetComponent(SpriteRenderer).sprite = bossImgs[0];
				toggleWobble(1,false);
				
				GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("changeToBossBG");
				
				
				phaseNum = 5;
				currAtkTime = Time.time;
				dTime=0;
				currSecond=0;
				triggerFlag=true;
			}
			
			break;
			
		case 5:
		
			if(currSecond==1 && triggerFlag){
				triggerFlag = false;
				concentrate();
			}
			else if(currSecond==4 && !triggerFlag){
				triggerFlag = true;
				isInvincible=false;
				InvokeRepeating("B1_P2",1f,1f);
				InvokeRepeating("B1_P1",0f,0.5f);
			}
			else if(currSecond==6 && triggerFlag){
				triggerFlag = false;
				hSpeed += 0.5f;
				//InvokeRepeating("B1_P1",0f,0.5f);
				
			}
			else if(currSecond==8 && !triggerFlag){
				triggerFlag = true;
				hSpeed += 0.5f;
				//InvokeRepeating("B1_P1",0f,0.5f);
				
			}
			else if(currSecond==10 && triggerFlag){
				triggerFlag = false;
				hSpeed += 0.5f;
				//InvokeRepeating("B1_P1",0f,0.5f);
				
			}
	}
	
}

function elaborateDeath(){
	gameObject.GetComponent.<CircleCollider2D>().enabled=false;
	var ring4 = Instantiate(effectRing,transform.position,Quaternion.identity);
	ring4.GetComponent("lerpScale").SendMessage("setMode","grow");
	flash();
	yield WaitForSeconds(0.5f);
	flash();
	yield WaitForSeconds(1f);
	flash();
	yield WaitForSeconds(1f);
	flash();
	
	hMin = transform.position.x - 0.1;
	hMax = transform.position.x + 0.1;
	hSpeed = 50f;
	hLerp = 0f;
	toggleWobble(0,true);
	
	GameObject.Find("bigBoom").GetComponent.<AudioSource>().Play();
	healthText.GetComponent(UI.Text).text = "Boss Health : Toast";
	gameObject.GetComponent(fadeScript).enabled = true;
	speed=0.5f;
	toAngle(270);
	
	yield WaitForSeconds(1f);
	flash();
	
	playerObj.GetComponent(spaceshipScript).score+=100;
	scoreText.GetComponent(UI.Text).text = "Score : "+playerObj.GetComponent(spaceshipScript).score;
	GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("blowUp",gameObject);
	GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("bossDied",gameObject);
	
	yield WaitForSeconds(1f);
	playerObj.GetComponent(spaceshipScript).SendMessage("levelComplete");
	Destroy(gameObject);
}

function flash(){
	GameObject.Find("flash").GetComponent.<Renderer>().enabled = true;
	yield WaitForSeconds(0.05f);
	GameObject.Find("flash").GetComponent.<Renderer>().enabled = false;
}

function concentrate(){
	GameObject.Find("woosh").GetComponent.<AudioSource>().Play();
	for(var ii=0;ii<5;ii++){
		var absorb = Instantiate(effectRing,transform.position,Quaternion.identity);
		absorb.GetComponent("lerpScale").SendMessage("setMode","shrink");
		yield WaitForSeconds(0.1f);
	}
}

function B1_P1(){
	var spawnPos = gameObject.transform.position;
	spawnPos.y -= 0.5f;
	GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("bossRing",spawnPos);
}

function B1_P2(){
	var spawnPos = gameObject.transform.position;
	spawnPos.y -= 0.5f;
	GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("bulletFan",spawnPos);
}

function OnTriggerEnter2D(obj : Collider2D) {  
    var name = obj.gameObject.name;
	if(!isInvincible){
		if (name == "bullet(Clone)") {
			GameObject.Find("enemyHit").GetComponent.<AudioSource>().Play();
			var bulletName = obj.gameObject.GetComponent(SpriteRenderer).sprite.name;
			if(bulletName == "shot_a_1"){health -= 1;}
			else if(bulletName == "shot_a_2"){health -= 2;}
			else if(bulletName == "shot_a_3"){health -= 3;}
			else if(bulletName == "shot_a_4"){health -= 4;}
			healthText.GetComponent(UI.Text).text = "Boss Health : "+health;
			
			//Mike's health bar here:
			//healthBar.size = health/totalHealth;

			Destroy(obj.gameObject);
			
			var currRenderer = GetComponent.<Renderer>();
			currRenderer.material.color = Color.red;
			yield WaitForSeconds(.05f);
			currRenderer.material.color = new Color(1,1,1,1);
			
			// enemy dies here, with all pertaining logic
			if(health<0){
				if(phaseNum==1){
					
					//var plusScore : int = (transform.localScale.x/1)+1;
					playerObj.GetComponent(spaceshipScript).score+=50;
					scoreText.GetComponent(UI.Text).text = "Score : "+playerObj.GetComponent(spaceshipScript).score;
					isInvincible=true;
					flash();
					GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("blowUp",gameObject);
					
					var ring = Instantiate(effectRing,transform.position,Quaternion.identity);
					ring.GetComponent("lerpScale").SendMessage("setMode","grow");
					
					health=500;
					healthText.GetComponent(UI.Text).text = "Boss Health : "+health;
					
					//Mike's health bar here:
					totalHealth = health;
					//healthBar.size = health/totalHealth;
					//Debug.Log ("TotalHealth " +totalHealth);
					//Debug.Log ("Division " +health/totalHealth);

					phaseNum=2;
					CancelInvoke("B1_P1");
					dTime=0;
					currSecond=0;
				}
				else if(phaseNum==3){
				
					//health=50;
					isInvincible=true;
					health=400;
					totalHealth=health;
					flash();
					var ring2 = Instantiate(effectRing,transform.position,Quaternion.identity);
					ring2.GetComponent("lerpScale").SendMessage("setMode","grow");
					
					phaseNum=4;
					CancelInvoke("B1_P2");
					dTime=0;
					currSecond=0;
					//toggleWobble(0); toggleWobble(1);
					triggerFlag=true;
					
					/*
					playerObj.GetComponent(spaceshipScript).score+=100;
					scoreText.GetComponent(UI.Text).text = "Score : "+playerObj.GetComponent(spaceshipScript).score;
					GameObject.Find("hud").GetComponent("levelOneScript").SendMessage("blowUp",gameObject);
					Destroy(gameObject);
					*/
				}
				else if(phaseNum==5){
					//toggleWobble(0); toggleWobble(1);
					GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("resetBG");
					CancelInvoke("B1_P2");
					CancelInvoke("B1_P1");
					toggleWobble(0,false); toggleWobble(1,false);
					elaborateDeath();
				}
			}
			  
		}
		
		if (name == "effectRing(Clone)"){
			health  -= 1;
			var renderer = GetComponent.<Renderer>();
			renderer.material.color = Color.red;
			yield WaitForSeconds(.05f);
			renderer.material.color = new Color(1,1,1,1);
			
			// enemy dies here, with all pertaining logic
			if(health<0){
				health=0;
				GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("blowUp",gameObject);
				//Destroy(gameObject);
				
			}
		}
		
		if (name == "spaceship") {
			var currPlayer : GameObject = GameObject.Find("spaceship");
			currPlayer.GetComponent("spaceshipScript").SendMessage("endLogic");
			//GameObject.Find("spaceship").GetComponent.<Renderer>().material.color = Color.red;
			//Time.timeScale = 0;        
			//AB_Destroy(gameObject);
		}
	}
	else{
		if (name == "bullet(Clone)") {	
			GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("singleBoom",obj.gameObject);
			Destroy(obj.gameObject);
		}
	}
}

function toAngle(angle : double){
	//var currRenderer = GetComponent("SpriteRenderer");
	//currRenderer.color = new Color(1,1,1,1);	

	var newDirection = Vector2(UnityEngine.Mathf.Cos(UnityEngine.Mathf.Deg2Rad*angle), UnityEngine.Mathf.Sin(UnityEngine.Mathf.Deg2Rad*angle));
	GetComponent.<Rigidbody2D>().velocity = newDirection * speed;
}

// 0 for H and 1 for V
function toggleWobble(which,value){
	if(which==0){
		if(value){
			//transform.position.x = Mathf.Lerp(hMin, hMax, 0.5);
			wobbleH=true;
		}
		else{
			wobbleH=false;
		}
	}
	else if(which==1){
		if(value){
			//transform.position.y = Mathf.Lerp(vMax, vMin, 0.5);
			wobbleV=true;
		}
		else{
			wobbleV=false;
		}
	}
}