﻿
public var speed : int = 6;

function Start () {  
	GetComponent.<Rigidbody2D>().velocity.y = speed;
	//Destroy(gameObject, 2);
}

function Update(){
	var viewPos : Vector3 = GameObject.Find("Main Camera").GetComponent("Camera").WorldToViewportPoint (
		gameObject.transform.position
		);
	if(viewPos.y > 1){
		Destroy(gameObject);
	}
}

function OnBecameInvisible() {  
    Destroy(gameObject);
}

function toAngle(angle : float){
	var newDirection = Vector2(UnityEngine.Mathf.Cos(UnityEngine.Mathf.Deg2Rad*angle), UnityEngine.Mathf.Sin(UnityEngine.Mathf.Deg2Rad*angle));
	GetComponent.<Rigidbody2D>().velocity = newDirection*speed;
	GetComponent.<Rigidbody2D>().angularVelocity = 10000;
}

function setSpeed(newSpeed : double){
	GetComponent.<Rigidbody2D>().velocity *= newSpeed;
}