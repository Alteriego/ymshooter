﻿#pragma strict

public var behaviorType="";
public var speed = 1f;

private var lerpColor1:Color;
private var lerpColor2:Color;

private var isFading:boolean=false;
private var lerpCounter:float=0f;

private var spriteRenderer:SpriteRenderer;
spriteRenderer = gameObject.GetComponent.<SpriteRenderer>();
private var rigidBody:Rigidbody2D;
rigidBody = gameObject.GetComponent.<Rigidbody2D>();

private var initialX=0f;
private var initialY=0f;
private var bgHeight=0f;
private var currAngle=0f;

private var origYScale=0f;
private var m7Flag:boolean=false;
public var m7First:boolean=false;

function Start () {
	
	//Debug.Log(gameObject.name+" - behavior is "+behaviorType);
	//Debug.Log(gameObject.name+" - rigidBody is "+rigidBody);
	
	if(behaviorType=="rotateRight"){
		fadeIn();
		//rigidBody.angularVelocity = 100;
		//GetComponent.<Rigidbody2D>().angularVelocity=30;
		//Debug.Log(gameObject.name+" - angularVelocity is "+rigidBody.angularVelocity);
	}
	else if(behaviorType=="rotateLeft"){
		fadeIn();
		//rigidBody.angularVelocity = -100;
		//GetComponent.<Rigidbody2D>().angularVelocity=100;
		//Debug.Log(gameObject.name+" - angularVelocity is "+rigidBody.angularVelocity);
	}
	else if(behaviorType=="scrollDown" || behaviorType=="scrollUp"){
		fadeIn();
		//rigidBody.angularVelocity = speed*300;
		initialX = transform.position.x;
		initialY = transform.position.y;
		bgHeight = spriteRenderer.bounds.max.y - spriteRenderer.bounds.min.y;
	}
	else if(behaviorType=="modeSeven"){
		origYScale = transform.localScale.y;
		if(!m7First){transform.localScale.y = 0;}else{next();}
		transform.localScale.x = 3;
		//spriteRenderer.color=lerpColor2;
	}
}

function Update () {
	if(isFading){
		lerpCounter += Time.deltaTime*0.5;
		spriteRenderer.color = Color.Lerp(lerpColor1, lerpColor2, lerpCounter);
		if(spriteRenderer.color==lerpColor2){
			isFading=false;
		}
	}
	if(behaviorType=="rotateRight"){
		transform.rotation = Quaternion.identity;
		currAngle += Time.deltaTime*speed*30;
		transform.Rotate(0,0,currAngle);
	}
	else if(behaviorType=="rotateLeft"){
		transform.rotation = Quaternion.identity;
		currAngle -= Time.deltaTime*speed*30;
		transform.Rotate(0,0,currAngle);
	}
	else if(behaviorType=="scrollDown"){
		transform.position.y -= Time.deltaTime*speed;
		if(transform.position.y <= (initialY-bgHeight)){
			transform.position.y = initialY;
		}
	}
	else if(behaviorType=="scrollUp"){
		transform.position.y += Time.deltaTime*speed;
		if(transform.position.y >= (initialY+bgHeight)){
			transform.position.y = initialY;
		}
	}
	else if(behaviorType=="modeSeven"){
		if(!m7Flag){
			transform.localScale.y += Time.deltaTime*speed;
			if(transform.localScale.y >= origYScale){
				next();
			}
		}
		else{
			var target = (Vector3.up*(speed*5)*-1) * Time.deltaTime;
			transform.Translate(target);
		}
	}
}

function next(){
	m7Flag=true;
	var nextPiece:GameObject = Instantiate(gameObject,gameObject.transform.position,gameObject.transform.rotation);
	nextPiece.GetComponent(complexBGScript).SendMessage("setAsM7");
	Destroy(gameObject,2f);
}

function fadeIn(){
	lerpColor1 = new Color(1,1,1,0);
	lerpColor2 = spriteRenderer.color;
	isFading = true;
}

function setAsM7(){
	behaviorType="modeSeven";
	m7Flag=false;
	m7First=false;
}