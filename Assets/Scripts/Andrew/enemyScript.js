﻿
public var avgSpeed : float = -20;
public var smallExplosion : GameObject;

private var actualSpeed : float;
private var hasBeenSeen : boolean = false;
private var health : double = 1;
private var origScale : Vector3;
private var willShoot : boolean = false;

private var isIndestruct : boolean = false;
private var isBombable : boolean = true;
private var isBullet = false;
private var isTurret = false;

private var pointValue = 0;

private var complexTrajectory:String="none";
private var complexTrajectoryOriginX:float=0f;
private var complexTrajectoryOriginY:float=0f;
private var complexTrajectoryVal1:float=0f;
private var complexTrajectoryVal2:float=0f;
private var complexTrajectoryVal3:float=0f;
private var complexTrajectoryVal4:float=0f;

private var simpleBG:GameObject;
simpleBG = GameObject.Find("bgSolid1");

private var anims : Sprite[];
private var origSpeed:float;

function Start () { 
    origScale = transform.localScale;
	origSpeed = avgSpeed;
	Debug.Log("avgSpeed = "+avgSpeed);
}

function OnEnable(){
	GetComponent.<Renderer>().enabled = true;
	GetComponent.<CircleCollider2D>().enabled = true;
}

function AB_Destroy(target){
	//Debug.Log("enemy died");
	isTurret=false; avgSpeed=origSpeed; //setSpeed(0);
	if(GetComponent(SpriteRenderer).sprite.name == "sideEnemy"){
		CancelInvoke("shotPatternA");
		gameObject.BroadcastMessage ("stopParticles");
	}
	CancelInvoke("shootRing");
	CancelInvoke("cactuarAnim");
	complexTrajectory="none";
	resetColor();
	resetSize();
	toAngle(0f);
	target.SetActive(false);
}	

function OnDisable(){
	GetComponent.<Renderer>().enabled = false;
}

function Update () {
	if(isTurret){
		syncToBG();
	}
	var viewPos : Vector3 = GameObject.Find("Main Camera").GetComponent("Camera").WorldToViewportPoint (
		gameObject.transform.position
		);
	if(viewPos.y > 1.5){
		AB_Destroy(gameObject);
	}
	if(complexTrajectory=="confetti"){
		//Debug.Log("updating confetti");
		var temp = 1f; if(complexTrajectoryVal1<0){temp=-1;}
		transform.position.x += Time.deltaTime*complexTrajectoryVal1;
		//var temp=1; if(complexTrajectoryVal1<0){temp=-1;}else{temp=1;}
		//transform.position.x += Time.deltaTime*temp*complexTrajectoryVal1;
		//transform.position.x += Time.deltaTime*1f;
		//transform.position.y *= -1.1;
		//Debug.Log(complexTrajectoryOriginX);
		var temp2 = complexTrajectoryVal1; if(temp2<0){temp2*=-1;}
		transform.position.y = complexTrajectoryOriginY-(Mathf.Pow((transform.position.x-complexTrajectoryOriginX),2f)*(0.25f/temp2)*6); 
	}
}

function syncToBG(){
	var currSpeed = simpleBG.GetComponent(scrollingBG).scrollSpeed;
	Debug.Log(currSpeed);
	//if(currSpeed<speed){
		avgSpeed = currSpeed;
		//GetComponent.<Rigidbody2D>().velocity.y = speed;
		toAngle(90);
	//}
}

function OnTriggerEnter (other : Collider) {
	Debug.Log("special collision!!");
}

function OnCollisionEnter(collision : Collision) {
	Debug.Log("special collision!!");
}

function OnTriggerEnter2D(obj : Collider2D) {  
	//Debug.Log("normal 2d collision! - "+obj.gameObject.name);
    var name = obj.gameObject.name;
    if (name == "bullet(Clone)" && !isIndestruct) {
		
		GameObject.Find("enemyHit").GetComponent.<AudioSource>().Play();
		
    	var bulletName = obj.gameObject.GetComponent(SpriteRenderer).sprite.name;
    	if(bulletName == "shot_a_1"){health -= 1;}
    	else if(bulletName == "shot_a_2"){health -= 2;}
    	else if(bulletName == "shot_a_3"){health -= 3;}
    	else if(bulletName == "shot_a_4"){health -= 4;}
    	
    	Destroy(obj.gameObject);
    	
    	var currRenderer = GetComponent("SpriteRenderer");
		currRenderer.color = Color.red;
		yield WaitForSeconds(.05f);
		currRenderer.color = new Color(1,1,1,1);
    	
		// enemy dies here, with all pertaining logic
        if(health<0){
			GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("blowUp",gameObject);
        	AB_Destroy(gameObject);
			
        	var playerObj : GameObject = GameObject.Find("spaceship");
        	//var plusScore : int = (transform.localScale.x/1)+1;
			playerObj.GetComponent("spaceshipScript").score+=pointValue;
			var scoreText : GameObject = GameObject.Find("ScoreText");
			scoreText.GetComponent("Text").text = "Score : "+playerObj.GetComponent("spaceshipScript").score;
		}
		  
    }

	if (name == "bulletWall"){
		if(isBullet){
			AB_Destroy(gameObject);
		}
	}
	
	if (name == "enemyWall"){
		//if(GetComponent(SpriteRenderer).sprite.name == "sideEnemy"){
			AB_Destroy(gameObject);
		//}
	}
	
	if (name == "effectRing(Clone)" && isBombable){
		//if(!isIndestruct){
			GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("blowUp",gameObject);
			AB_Destroy(gameObject);
		//}
	}
	
    if (name == "spaceship" || name == "player2dhitbox") {
		Debug.Log("hit!");
		var currPlayer : GameObject = GameObject.Find("spaceship");
		currPlayer.GetComponent("spaceshipScript").SendMessage("takeDmg",1);
		//PlayerPrefs.SetInt(highScoreKey, score);
        //PlayerPrefs.Save();
        //GameObject.Find("spaceship").GetComponent.<Renderer>().material.color = Color.red;
        //Time.timeScale = 0;        
        AB_Destroy(gameObject);
    }
	
}

function changeSpriteTo(newSprite:Sprite){
	GetComponent(SpriteRenderer).sprite = newSprite;
}

function setIndestruct(value:boolean){
	isIndestruct = value;
}

function setPointValue(value:int){
	pointValue = value;
}

function makeSpin(speed){
	var newSpeed = 0;
	if(speed!=-1){newSpeed = speed;}
	else{newSpeed = Random.Range(-200,200);}
	GetComponent.<Rigidbody2D>().angularVelocity = newSpeed;
}

function rotateToAngle(angle:double){
	transform.rotation = Quaternion.identity;
	transform.Rotate(0,0,angle);
}

function toAngle(angle : double){
	complexTrajectory="none";
	//var currRenderer = GetComponent("SpriteRenderer");
	//currRenderer.color = new Color(1,1,1,1);	

	var newDirection = Vector2(UnityEngine.Mathf.Cos(UnityEngine.Mathf.Deg2Rad*angle), UnityEngine.Mathf.Sin(UnityEngine.Mathf.Deg2Rad*angle));
	GetComponent.<Rigidbody2D>().velocity = newDirection * avgSpeed;
}

function setAvgSpeed(newSpeed : double){
	avgSpeed = newSpeed;
}

function setSpeed(newSpeed : double){
	GetComponent.<Rigidbody2D>().velocity *= newSpeed;
}

function resetSize(){
	transform.localScale = new Vector3(0.5f,0.5f,0.5f);
}

function setColor(newColor : Color){
	var currRenderer = GetComponent("SpriteRenderer");
	currRenderer.color = newColor;
}

function resetColor(){
	var currRenderer = GetComponent("SpriteRenderer");
	currRenderer.color = new Color(1,1,1,1);
}

function setSize(newSize : double){
	//transform.localScale = origScale;
	//setHealth(newSize);
	resetSize();
	transform.localScale = new Vector3(newSize,newSize,newSize);	
}

function setHealth(newHealth : double){
	health = newHealth;
}

function becomeDebris(){
	transform.rotation = Quaternion.identity;
	angleDeflect = Random.Range(1,15);
	newAngle = -(Random.Range(270-angleDeflect,270+angleDeflect));
	newSpeed = Random.Range(1.5f,3f);
	newSize = Random.Range(0.25,1.5f);
	makeSpin(-1);
	setSize(newSize);
	if(newSize>1){setPointValue(2);}else{setPointValue(1);}
	setHealth(2*newSize);
	toAngle(newAngle);
	setSpeed(newSpeed);
	setIndestruct(false);
	setIsBullet(true);
}

function becomeSideEnemy(isLeft:boolean){
	resetSize();
	transform.rotation = Quaternion.identity;
	if(isLeft){
		newAngle = 180;
		transform.rotation.y = 0;
	}
	else{
		newAngle = 0;
		transform.rotation.y = 180;
	}
	toAngle(newAngle);
	setSpeed(Random.Range(3,5));
	setHealth(3);
	InvokeRepeating("shotPatternA",Random.Range(0.75f,1.25f),Random.Range(1.75f,2.25f));
	setIndestruct(false);
	setIsBullet(false);
}

function shotPatternA(){
	if(transform.position.x > 0.637 && transform.position.x < 4.074){
		var randShotPat = Random.Range(0f,1f);
		if (randShotPat<0.5){shootRing();}
		else {shootFan();}
	}
}

function shootRing(){
	var gameControllerObject : GameObject = GameObject.FindWithTag("GameController");
	gameControllerObject/*.GetComponent("levelOneScript")*/.SendMessage("bulletRing",transform.position);
}

function shootFan(){
	var gameControllerObject : GameObject = GameObject.FindWithTag("GameController");
	gameControllerObject/*.GetComponent("levelOneScript")*/.SendMessage("bulletFan",transform.position);
}

function shootLaser(){
	var gameControllerObject : GameObject = GameObject.FindWithTag("GameController");
	gameControllerObject/*.GetComponent("levelOneScript")*/.SendMessage("bulletLaser",transform.position);
}

function setIsBullet(value:boolean){
	isBullet = value;
}

function setIsBombable(value:boolean){
	isBombable = value;
}

function delayedTurn(angle:double){
	yield WaitForSeconds(0.2f);
	toAngle(angle);
	rotateToAngle(angle);
	setSpeed(2);
}

function delayedTurnHalfSec(angle:double){
	yield WaitForSeconds(0.5f);
	toAngle(angle);
	rotateToAngle(angle);
	setSpeed(2);
}

function setWillShoot(value:boolean){
	willShoot = value;
}

function becomeConfetti(newDir:float){
	resetColor();
	avgSpeed=-0.4;
	setSpeed(0f);
	complexTrajectoryOriginX = transform.position.x;
	complexTrajectoryOriginY = transform.position.y;
	complexTrajectory="confetti";
	complexTrajectoryVal1=newDir*1f;
	if(newDir==0){AB_Destroy(gameObject);}
}

function becomeDropEnemy(){
	transform.rotation = Quaternion.identity;
	resetSize();
	toAngle(90);
	//setSpeed(4);
	setIndestruct(false);
	setIsBullet(false);
	if(willShoot){
		yield WaitForSeconds(0.5f);
		//GameObject.Find("enemyShot").GetComponent.<AudioSource>().Play();
		GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("bulletRing",transform.position);
	}
}

function becomeCactuar(){
	transform.rotation = Quaternion.identity;
	anims = Resources.LoadAll.<Sprite>("cactuar");	
	
	changeSpriteTo(anims[0]);
	
	isTurret=true;
	setIndestruct(false);
	setIsBullet(false);
	setHealth(7);
	setPointValue(7);
	setSize(0.25f);
	InvokeRepeating("shootRing",0.6f,2f);
	InvokeRepeating("cactuarAnim",0f,0.25f);
}

function cactuarAnim(){
	if(GetComponent(SpriteRenderer).sprite == anims[0]){GetComponent(SpriteRenderer).sprite=anims[1];}
	else if(GetComponent(SpriteRenderer).sprite == anims[1]){GetComponent(SpriteRenderer).sprite=anims[0];}
}

function becomeDropObstacle(){
	transform.rotation = Quaternion.identity;
	resetSize();
	toAngle(90);
	//setSpeed(4);
	//setSize(1);
	setIndestruct(true);
	setIsBullet(false);
	setColor(Color.red);
}

function becomeYoyoEnemy(){
	transform.rotation = Quaternion.identity;
	
	resetSize();
	setSize(1f);
	toAngle(90);
	setSpeed(4);
	setIndestruct(false);
	setIsBullet(false);
	yield WaitForSeconds(0.5f);
	setSpeed(0);
	yield WaitForSeconds(0.25f);
	shootLaser();
	yield WaitForSeconds(1f);
	toAngle(270);
	setSpeed(8);
}

function delayedSpeedDown(delay:double){
	yield WaitForSeconds(delay);
	setSpeed(0.1f);
}