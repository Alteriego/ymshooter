﻿#pragma strict

function Start () {
	//delayedDeath();
}

function Update () {

}

function delayedDeath(){
	yield WaitForSeconds(0.2f);
	AB_Destroy(gameObject);
}

function OnEnable(){
	GetComponent.<Renderer>().enabled = true;
	delayedDeath();
}

function AB_Destroy(target:GameObject){
	target.SetActive(false);
}	

function OnDisable(){
	GetComponent.<Renderer>().enabled = false;
}

function setSize(newSize : Vector3){
	resetSize();
	transform.localScale = newSize*2;	
}

function resetSize(){
	transform.localScale = new Vector3(0.5f,0.5f,0.5f);
}
