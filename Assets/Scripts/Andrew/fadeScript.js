﻿#pragma strict

public var A:Color = new Color(1,1,1,0);
public var B:Color = new Color(1,1,1,1);
public var speed:float = 1.0f;
	
public var mode = "idle";
	
private var spriteRenderer:SpriteRenderer;
private var tempValue:float = 0f;
	
function Start() {
	spriteRenderer = gameObject.GetComponent.<SpriteRenderer>();
	if(mode=="idle"){fadeIn();}
}

function Update(){
	
	if(mode=="pingpong"){
		spriteRenderer.color = Color.Lerp(A, B, Mathf.PingPong(Time.time * speed, 1.0f));
	}
	
	else if (mode=="fadeIn"){
		tempValue += Time.deltaTime * speed;
		spriteRenderer.color = Color.Lerp(A, B, tempValue);
		if(tempValue>=1f){
			mode =  "idle";
		}
	}
	
	else if (mode=="idle"){}
}

function fadeIn(){
	spriteRenderer.color = A;
	tempValue = 0f;
	mode = "fadeIn";
}

