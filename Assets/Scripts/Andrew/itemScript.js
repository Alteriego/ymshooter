﻿
public var itemSpeed : float = -3;

private var hasItemBeenSeen : boolean = false;

private var effect = "default";
private var itemImgs : Sprite[];


function Start () {  
	
	//Debug.Log("itemImgs length="+itemImgs.Length);
	//Debug.Log(itemImgs[0]);
	GetComponent.<Rigidbody2D>().velocity.y = itemSpeed;
}

function Update () {
	if(GetComponent.<Renderer>().isVisible){ 
		hasBeenSeen = true; 
	}
	if(hasBeenSeen && !GetComponent.<Renderer>().isVisible){ 
		Destroy(gameObject);	 
	}
}

function OnTriggerEnter2D(obj : Collider2D) {  
    
    var name = obj.gameObject.name;

    if (name == "spaceship" || name == "player2dhitbox") {

		var playerObj : GameObject = GameObject.Find("spaceship");
		
		if(effect == "powUp"){
			GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("powUpSound");
			
			
			if(playerObj.GetComponent("spaceshipScript").wepNum < 6){
				playerObj.GetComponent("spaceshipScript").wepXp+=1;
				if(playerObj.GetComponent("spaceshipScript").wepXp==1){
					playerObj.GetComponent("spaceshipScript").wepXp=0;
					playerObj.GetComponent("spaceshipScript").wepNum+=1;
				}
			}
		}
		else if(effect == "hyper"){
			//GameObject.FindWithTag("GameController").GetComponent("levelOneScript").SendMessage("hyperSound");
			playerObj.GetComponent(spaceshipScript).SendMessage("goHyper",7f);
		}
		
		Destroy(gameObject);
    }
}

function setAsPowUp(){
	Debug.Log("set as pow up!");
	itemImgs = Resources.LoadAll.<Sprite>("items");
	//Debug.Log(itemImgs[0]);
	GetComponent(SpriteRenderer).sprite = itemImgs[0];
	effect = "powUp";
}

function setAsHyper(){
	Debug.Log("set as hyper");
	itemImgs = Resources.LoadAll.<Sprite>("items");
	GetComponent(SpriteRenderer).sprite = itemImgs[1];
	effect = "hyper";
}