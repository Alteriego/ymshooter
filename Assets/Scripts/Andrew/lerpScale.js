﻿#pragma strict

public var startSize = 1;
public var endSize = 10;
public var speed = 10f;

private var mode = "default";

function Start () {
	//setMode("grow");
}

function Update () {
	if(mode!="default"){
	
		if(mode=="grow"){
			transform.localScale += Vector3(speed,speed,speed)*Time.deltaTime;
		}
		if(mode=="shrink"){
			transform.localScale -= Vector3(speed,speed,speed)*Time.deltaTime;
			if(transform.localScale.x<=0.25f){
				Destroy(gameObject);
			}
		}
	}
}

function setMode(newMode:String){
	mode = newMode;
	var gameControllerObject= GameObject.FindWithTag("GameController");
	if(mode=="grow"){
		gameControllerObject/*.GetComponent("levelOneScript")*/.SendMessage("bombSound");
		Destroy(gameObject,1);
	}
	else if(mode=="shrink"){
		//hudObj.GetComponent("levelOneScript").SendMessage("wooshSound");
		transform.localScale *= 20;
	}
}
