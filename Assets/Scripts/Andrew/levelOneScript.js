﻿#pragma strict

public var worldEnemy : GameObject;
public var powUp : GameObject;
public var explosion : GameObject;
public var boss1 : GameObject;

private var currentSecond = 1;
private var worldEnemyList : GameObject[] = new GameObject[500];
private var worldEffectList : GameObject[] = new GameObject[250];
private var ringSpawnSpeed = 4;
private var bulletsInRing = 30;
private var shotImgs : Sprite[];
private var enemyImgs : Sprite[];
private var sfx : AudioClip[];

private var newTime=0f;
private var bossSpawned:boolean=false;

private var simpleBG:GameObject;
simpleBG = GameObject.Find("BG");
private var complexBG:GameObject;
complexBG = GameObject.Find("complexBG");
complexBG.SetActive(false);

//Mike's health bars here:
private var playerHP : UnityEngine.UI.Scrollbar;
playerHP = GameObject.Find("PlayerHealthBar").GetComponent(UnityEngine.UI.Scrollbar);
private var bossHP : UnityEngine.UI.Scrollbar;
bossHP = GameObject.Find("BossHealthBar").GetComponent(UnityEngine.UI.Scrollbar);

function Start () {
	//simpleBG.SetActive(false);
	//complexBG.SetActive(true);
	for(var ii=0;ii<worldEnemyList.Length;ii++){
		var obj = Instantiate(worldEnemy);
		obj.GetComponent.<Renderer>().sortingOrder=1;
		obj.SetActive(false);
		worldEnemyList[ii] = obj;
	}
	for(var jj=0;jj<worldEffectList.Length;jj++){
		var obj2 = Instantiate(explosion);
		obj2.SetActive(false);
		worldEffectList[jj] = obj2;
	}
	shotImgs = Resources.LoadAll.<Sprite>("tyrian");
	enemyImgs = Resources.LoadAll.<Sprite>("enemyShips");
	sfx = Resources.LoadAll.<AudioClip>("Audio/SFX");
	
}

function Update () {

	newTime += (Time.deltaTime * 1);
	//Debug.Log("newTime="+newTime);
	
	handleGUIStuff();
	
	//var temp = Mathf.Floor(UnityEngine.Time.timeSinceLevelLoad); 
	if(newTime>currentSecond){
		currentSecond++;
		//Debug.Log("currentSecond="+currentSecond);
		
		
		
		// Timed Waves start here!!
		
		if(currentSecond==2){
			InvokeRepeating("spawnDebris", 0f, 0.5f);
			//powUpChance(true);
			/*
			var bossSpawn = GameObject.Find("playTopOut").transform.position;
			bossSpawn.y += 1f;
			Instantiate(boss1,bossSpawn,Quaternion.identity);
			bossSpawned=true;
			*/
		}
		
		else if(currentSecond==12){
			CancelInvoke("spawnDebris");
		}
		
		else if(currentSecond==14){
			powUpChance(true);
			InvokeRepeating("spawnWave1",3f,1.5f);
		}
		
		else if(currentSecond==25){
			CancelInvoke("spawnWave1");
		}
		
		else if(currentSecond==27){
			spawnWave1_5();			
		}
		
		else if(currentSecond==30){
			//InvokeRepeating("spawnWave2_2",0.5f,1f);
			var bossSpawn = GameObject.Find("playTopOut").transform.position;
			bossSpawn.y += 1f;
			Instantiate(boss1,bossSpawn,Quaternion.identity);
			bossSpawned=true;
		}
		/*
		else if(currentSecond==35){
			InvokeRepeating("spawnWave2_1",0f,1f);
		}
		
		else if(currentSecond==40){
			CancelInvoke("spawnWave2_1");
			CancelInvoke("spawnWave2_2");
		}
		
		else if(currentSecond==42){
			var bossSpawn = GameObject.Find("playTopOut").transform.position;
			bossSpawn.y += 1f;
			Instantiate(boss1,bossSpawn,Quaternion.identity);
		}
		*/
	} 
	
}

function bossDied(){
	bossSpawned=false;
}

function handleGUIStuff(){
	var player = GameObject.Find("spaceship").GetComponent(spaceshipScript);
	playerHP.size = player.currHealth/player.maxHealth;
	if(bossSpawned){
		var boss = GameObject.Find("bossOne(Clone)").GetComponent(boss1Script);
		//Debug.Log(boss.health);
		bossHP.size = boss.health/boss.totalHealth;
	}
}

function wooshSound(){
	//GameObject.Find("pewpew").GetComponent.<AudioSource>().clip = sfx[6];
	GameObject.Find("woosh").GetComponent.<AudioSource>().Play();
}

function bombSound(){
	//GameObject.Find("pewpew").GetComponent.<AudioSource>().clip = sfx[6];
	GameObject.Find("bomb").GetComponent.<AudioSource>().Play();
}

function pewpew(){
	//GameObject.Find("pewpew").GetComponent.<AudioSource>().clip = sfx[6];
	GameObject.Find("pewpew").GetComponent.<AudioSource>().Play();
}

function powUpSound(){
	//GameObject.Find("pewpew").GetComponent.<AudioSource>().clip = sfx[4];
	GameObject.Find("powUpSound").GetComponent.<AudioSource>().Play();
}

function hyperSound(){
	for(var ii=0;ii<4;ii++){
		GameObject.Find("deepPew").GetComponent.<AudioSource>().Play();
		yield WaitForSeconds(0.05f);
	}
}

function changeToBossBG(){
	var simpleBGs = GameObject.FindGameObjectsWithTag("simpleBG");
	if(simpleBGs){
		for(var ii=0;ii<simpleBGs.Length;ii++){
			simpleBGs[ii].GetComponent(SpriteRenderer).enabled = false;
		}
	}
	complexBG.SetActive(true);
}

function resetBG(){
	complexBG.SetActive(false);
	var simpleBGs = GameObject.FindGameObjectsWithTag("simpleBG");
	if(simpleBGs){
		for(var ii=0;ii<simpleBGs.Length;ii++){
			simpleBGs[ii].GetComponent(SpriteRenderer).enabled = true;
		}
	}
}

function powUpChance(guaranteed:boolean){
	if(!guaranteed){
		var rand = Random.Range(0f,10f);
		if (rand<0.1f){	
			var x1_pow = transform.position.x - GetComponent.<Renderer>().bounds.size.x/2;
			var x2_pow = transform.position.x + GetComponent.<Renderer>().bounds.size.x/2;
			var powSpawn = new Vector2(Random.Range(x1_pow, x2_pow), transform.position.y);
			var newPow:GameObject = Instantiate(powUp, powSpawn, Quaternion.identity);
			newPow.GetComponent(itemScript).SendMessage("setAsPowUp");
		}
	}
	else{
		var spawnPoint = GameObject.Find("playTopOut").transform.position;
		var newPowUp:GameObject = Instantiate(powUp, spawnPoint, Quaternion.identity);
		//newPowUp.GetComponent(itemScript).SendMessage("setAsPowUp");
		newPowUp.GetComponent(itemScript).SendMessage("setAsPowUp");
	}
}

function dropHyperPowUp(position:Vector3){
	var newPowUp:GameObject = Instantiate(powUp, position, Quaternion.identity);
	newPowUp.GetComponent(itemScript).SendMessage("setAsHyper");
}

function blowUp(target:GameObject){
	
	//GameObject.Find("pewpew").GetComponent.<AudioSource>().clip = sfx[7];
	
	
	for(var ii=0; ii<5; ii++){
		var randPosition = target.transform.position;
		randPosition.x += Random.Range(-0.1f,0.1f);
		randPosition.y += Random.Range(-0.1f,0.1f);
		for(var jj=0; jj<worldEffectList.Length; jj++){
			if(!worldEffectList[jj].activeInHierarchy){
				worldEffectList[jj].SetActive(true);
				worldEffectList[jj].transform.position = randPosition;
				worldEffectList[jj].GetComponent("explosionScript").SendMessage("setSize",target.transform.localScale);
				break;
			}
		}
		//var splode = Instantiate(smallExplosion, randPosition, Quaternion.identity);
		//splode.transform.localScale = transform.localScale*2;
		//Destroy(splode,0.2f);
		GameObject.Find("smallBoom").GetComponent.<AudioSource>().Play();
		yield WaitForSeconds(0.05f);
	}
}

function singleBoom(target:GameObject){
	
	//GameObject.Find("pewpew").GetComponent.<AudioSource>().clip = sfx[7];
	
	
	
		var randPosition = target.transform.position;
		//randPosition.x += Random.Range(-0.1f,0.1f);
		//randPosition.y += Random.Range(-0.1f,0.1f);
		for(var jj=0; jj<worldEffectList.Length; jj++){
			if(!worldEffectList[jj].activeInHierarchy){
				worldEffectList[jj].SetActive(true);
				worldEffectList[jj].transform.position = randPosition;
				worldEffectList[jj].GetComponent("explosionScript").SendMessage("setSize",target.transform.localScale);
				break;
			}
		}
		//var splode = Instantiate(smallExplosion, randPosition, Quaternion.identity);
		//splode.transform.localScale = transform.localScale*2;
		//Destroy(splode,0.2f);
		GameObject.Find("smallBoom").GetComponent.<AudioSource>().Play();
		//yield WaitForSeconds(0.05f);
	
}

function bossRing (position:Vector3){
	var numBullets = 20;
	
	/*var randomPos = GameObject.Find("playCenter").transform.position;
	randomPos.x += Random.Range(-1.0,1.0);
	randomPos.y += Random.Range(1.0,2.0);*/
	GameObject.Find("enemyShot").GetComponent.<AudioSource>().Play();
	
	var randomShot = Random.Range(5,100);
	var randDir = Random.Range(0f,1f);
	if (randDir<0.5){randDir = 1;}else{randDir = -1;}
	
	for(var ii=0; ii<numBullets; ii++){
		for(var jj=0; jj<worldEnemyList.Length; jj++){
			if(!worldEnemyList[jj].activeInHierarchy){
				worldEnemyList[jj].SetActive(true);

				worldEnemyList[jj].transform.position = position;
				
				var newAngle = 0+(360/numBullets)*ii+Random.Range(0f,15f);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("resetSize");
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("toAngle",newAngle);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("rotateToAngle",newAngle);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setSpeed",4.0);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("changeSpriteTo",shotImgs[14]);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIndestruct",true);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIsBullet",true);
				//worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIsBombable",true);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("delayedTurn", newAngle-(190*randDir));
				break;
			}
		}	
	}
}

function bulletRing (position:Vector3){
	var numBullets = 3;
	
	/*var randomPos = GameObject.Find("playCenter").transform.position;
	randomPos.x += Random.Range(-1.0,1.0);
	randomPos.y += Random.Range(1.0,2.0);*/
	GameObject.Find("enemyShot").GetComponent.<AudioSource>().Play();
	
	var randomShot = Random.Range(5,100);
	var randDir = Random.Range(1f,1f);
	if (randDir<0.5){randDir = 1;}else{randDir = -1;}
	
	for(var ii=0; ii<numBullets; ii++){
		for(var jj=0; jj<worldEnemyList.Length; jj++){
			if(!worldEnemyList[jj].activeInHierarchy){
				worldEnemyList[jj].SetActive(true);

				worldEnemyList[jj].transform.position = position;
				
				var newAngle = 0+(360/numBullets)*ii+Random.Range(0f,15f);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("resetSize");
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("toAngle",newAngle);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("rotateToAngle",newAngle);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setSpeed",4.0);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("changeSpriteTo",shotImgs[254]);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIndestruct",true);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIsBullet",true);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("delayedTurn", newAngle-(210*randDir));
				break;
			}
		}	
	}
}

function bulletFan (position:Vector3){
	var numBullets = 5;
	var fanWidth = 15;

	var myPosition = position;
	var playerPosition = GameObject.Find("spaceship").transform.position;
	
	GameObject.Find("enemyShot").GetComponent.<AudioSource>().Play();
	
	var deltaY = playerPosition.y - myPosition.y;
	var deltaX = playerPosition.x - myPosition.x;
	var angleToPlayer = Mathf.Atan2(deltaX,deltaY) * Mathf.Rad2Deg;
	Debug.Log(angleToPlayer);
	angleToPlayer = angleToPlayer*(-1)-90;
	
	for(var aa=0;aa<6;aa++){
		for(var ii=0; ii<numBullets; ii++){
			for(var jj=0; jj<worldEnemyList.Length; jj++){
				if(!worldEnemyList[jj].activeInHierarchy){
					worldEnemyList[jj].SetActive(true);

					worldEnemyList[jj].transform.position = myPosition;
					
					var newAngle = angleToPlayer + ( (ii-(numBullets/2)) * (fanWidth/numBullets) ) ;
					
					newAngle += (60*aa);
					
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("resetSize");
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("toAngle",newAngle);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("rotateToAngle",newAngle+90);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setSpeed",4.0);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("changeSpriteTo",enemyImgs[4]);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIndestruct",true);
					//worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setHealth",5);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIsBullet",true);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setPointValue", 1);
					break;
				}
			}	
		}
	}
}

function bulletLaser (position:Vector3){
	var laserLength = 15;
	var laserWidth = 30;
	var laserSpeed = 30;

	var myPosition = position;
	var playerPosition = GameObject.Find("spaceship").transform.position;
	
	var deltaY = playerPosition.y - myPosition.y;
	var deltaX = playerPosition.x - myPosition.x;
	var angleToPlayer = Mathf.Atan2(deltaX,deltaY) * Mathf.Rad2Deg;
	Debug.Log(angleToPlayer);
	angleToPlayer = angleToPlayer*(-1)-90;
	
	for(var ii=0; ii<laserLength; ii++){
		for(var fanLoop=0; fanLoop<3; fanLoop++){
			for(var jj=0; jj<worldEnemyList.Length; jj++){
				if(!worldEnemyList[jj].activeInHierarchy){
					worldEnemyList[jj].SetActive(true);

					worldEnemyList[jj].transform.position = myPosition;
					
					var newAngle = angleToPlayer+(-7+(7*fanLoop));
					
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("resetSize");
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("toAngle",newAngle);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("rotateToAngle",newAngle);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setSpeed",laserSpeed);
					
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("changeSpriteTo",shotImgs[8]);
					
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIndestruct",true);
					//worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setHealth",5);
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIsBullet",true);
					//worldEnemyList[jj].GetComponent("enemyScript").SendMessage("delayedTurn", angleToPlayer);
					
					worldEnemyList[jj].GetComponent("enemyScript").SendMessage("delayedSpeedDown", 0.1f);
					break;
				}
			}
		}
		yield WaitForSeconds(0.025f);
	}
}

function obstacleWall(numBullets:int, speed:int, health:int, variance:float, sprite:Sprite, scale:float, willShoot:boolean){
	//var variance = Random.Range(-0.25f,0.25f);
	var horLen = GameObject.Find("playRightOut").transform.position.x - GameObject.Find("playLeftOut").transform.position.x;
	Debug.Log(horLen);
	for(var ii=0; ii<numBullets; ii++){
		var spawnPos = GameObject.Find("playTopOut").transform.position;
		spawnPos.x = GameObject.Find("playLeftOut").transform.position.x;
		spawnPos.x += ((horLen/(numBullets * 1f))*ii)+((horLen/(numBullets * 1f))/2)+variance;
		
		if(health>0){spawnDropEnemy(spawnPos,sprite,speed,health,scale,willShoot);}
		else{spawnDropObstacle(spawnPos,sprite,speed,scale);}
	}
}

function spawnSideEnemy(){
	var randomSide = Random.Range(0f,1f);
	var isLeft = randomSide<0.5f;
	var randomPos = Vector3.zero;
	if(isLeft){
		randomPos = GameObject.Find("playLeftOut").transform.position;
	}
	else{
		randomPos = GameObject.Find("playRightOut").transform.position;
	} 
	randomPos.y += Random.Range(1,2f);
	
	for(var jj=0; jj<worldEnemyList.Length; jj++){
		if(!worldEnemyList[jj].activeInHierarchy){
			worldEnemyList[jj].SetActive(true);
			worldEnemyList[jj].transform.position = randomPos;
			
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("changeSpriteTo",enemyImgs[3]);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("becomeSideEnemy",isLeft);
			
			break;
		}
	}
}

function spawnDropEnemy(position:Vector3,sprite:Sprite,speed:int,health:int,scale:float,willShoot:boolean){
	
	var randomPos = Vector3.zero;
	
	if(position!=null){randomPos = position;}
	else{
		randomPos = GameObject.Find("playTopOut").transform.position;
		randomPos.x += Random.Range(-1f,1f);
	}
	
	for(var jj=0; jj<worldEnemyList.Length; jj++){
		if(!worldEnemyList[jj].activeInHierarchy){
			worldEnemyList[jj].SetActive(true);
			worldEnemyList[jj].transform.position = randomPos;
			
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("changeSpriteTo",sprite);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setWillShoot",willShoot);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("becomeDropEnemy");
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("rotateToAngle",180);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setHealth",health);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setSpeed",speed);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setSize",scale);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setPointValue", 1);
			break;
		}
	}
}

function spawnDropObstacle(position:Vector3,sprite:Sprite,speed:int,scale:float){
	
	var randomPos = Vector3.zero;
	
	if(position!=null){randomPos = position;}
	else{
		randomPos = GameObject.Find("playTopOut").transform.position;
		randomPos.x += Random.Range(-1f,1f);
	}
	
	for(var jj=0; jj<worldEnemyList.Length; jj++){
		if(!worldEnemyList[jj].activeInHierarchy){
			worldEnemyList[jj].SetActive(true);
			worldEnemyList[jj].transform.position = randomPos;
			
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("changeSpriteTo",sprite);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("becomeDropObstacle");
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("makeSpin",500);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setSpeed",speed);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setSize",scale);
			worldEnemyList[jj].GetComponent("enemyScript").SendMessage("setIsBombable",false);
			break;
		}
	}
}

function spawnDebris(){
	
	var randomPos = Vector3.zero;
	randomPos = GameObject.Find("playTopOut").transform.position;
	
	for(var ii=0; ii<(currentSecond/2); ii++){
	
		randomPos.x += Random.Range(-1f,1f);
	
		for(var jj=0; jj<worldEnemyList.Length; jj++){
			if(!worldEnemyList[jj].activeInHierarchy){
				worldEnemyList[jj].SetActive(true);
				worldEnemyList[jj].transform.position = randomPos;
				
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("changeSpriteTo",enemyImgs[0]);
				worldEnemyList[jj].GetComponent("enemyScript").SendMessage("becomeDebris");
				
				break;
			}
		}
	}
}

function spawnWave1(){
	var numBullets = 15;
	var variance = Random.Range(-0.25f,0.25f);
	var speed = 4;
	var health = 7;
	var scale = 0.5;
	var willShoot = true;
	obstacleWall(numBullets, speed, health, variance, enemyImgs[4], scale, willShoot);
}

function spawnWave1_5(){
	var numBullets = 3;
	var variance = 0;
	var speed = 4;
	var health = 0;
	var scale = 1.75;
	var willShoot = false;
	obstacleWall(numBullets,speed,health,variance,enemyImgs[2],scale,willShoot);
}

function spawnWave2_1(){
	var numBullets = 20;
	var variance = Random.Range(-0.25f,0.25f);
	var speed = 5;
	var health = 2;
	var scale = 0.25;
	var willShoot = false;
	obstacleWall(numBullets, speed, health, variance, enemyImgs[4], scale, willShoot);
}

function spawnWave2_2(){
	var numBullets = 7;
	var variance = Random.Range(-0.25f,0.25f);
	var speed = 3;
	var health = 0;
	var scale = 0.5;
	var willShoot = false;
	obstacleWall(numBullets, speed, health, variance, enemyImgs[2], scale, willShoot);
}
