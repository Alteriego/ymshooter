﻿#pragma strict

public var speed:double=-1f;

private var bgObj:GameObject;
bgObj = GameObject.Find("bgSolid1");



private var canCollide:boolean=true;

function Start () {
	//decorationImgs = Resources.LoadAll.<Sprite>("desertDecorations");
	
	toAngle(90);
}

function Update () {
	syncToBG();
}

function OnTriggerEnter2D(obj : Collider2D) {  
	Debug.Log("normal 2d collision! - "+obj.gameObject.name);
    var name = obj.gameObject.name;
    if(canCollide){
		if (name == "bullet(Clone)") {
			
			GameObject.FindWithTag("GameController")/*.GetComponent("levelOneScript")*/.SendMessage("singleBoom",obj.gameObject);
			Destroy(obj.gameObject);
			  
		}
		
		
		
		if (name == "spaceship" || name == "player2dhitbox") {
			Debug.Log("hit!");
			var currPlayer : GameObject = GameObject.Find("spaceship");
			currPlayer.GetComponent("spaceshipScript").SendMessage("takeDmg",1);
			//PlayerPrefs.SetInt(highScoreKey, score);
			//PlayerPrefs.Save();
			//GameObject.Find("spaceship").GetComponent.<Renderer>().material.color = Color.red;
			//Time.timeScale = 0;        
			//AB_Destroy(gameObject);
		}
	}
	if (name == "enemyWall"){
		//if(GetComponent(SpriteRenderer).sprite.name == "sideEnemy"){
			Destroy(gameObject);
		//}
	}
	
}

function becomeDecoration(newSprite:Sprite){
	
	//Debug.Log(decorationImgs[2]);
	
	canCollide=false;
	GetComponent(SpriteRenderer).sprite = newSprite;
}

function toAngle(angle : double){
	var newDirection = Vector2(UnityEngine.Mathf.Cos(UnityEngine.Mathf.Deg2Rad*angle), UnityEngine.Mathf.Sin(UnityEngine.Mathf.Deg2Rad*angle));
	GetComponent.<Rigidbody2D>().velocity = newDirection * speed;
}

function syncToBG(){
	var currSpeed = bgObj.GetComponent(scrollingBG).scrollSpeed;
	Debug.Log(currSpeed);
	//if(currSpeed<speed){
		speed = currSpeed;
		//GetComponent.<Rigidbody2D>().velocity.y = speed;
		toAngle(90);
	//}
}