﻿#pragma strict

private var isDead:boolean=false;

private var myParticles:ParticleSystem;
myParticles = GetComponent(ParticleSystem);

function Start () {
	Debug.Log(myParticles);
}

function Update () {
	if(isDead){
		if(myParticles.particleCount<=0){
			Destroy(gameObject,3f);
		}
	}
}

function stopParticles(){
	Debug.Log("stop!");
	transform.parent = null;
	myParticles.emissionRate = 0;
	isDead=true;
}