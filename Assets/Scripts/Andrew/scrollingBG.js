﻿
public var scrollSpeed : float = -1;
public var isModeSeven : boolean = false;

private var initialX : float;
private var initialY : float;
private var initialZ : float;
private var bgHeight : float;
private var dist : float = 0;

function Start () {
	initialX = transform.position.x;
	initialY = transform.position.y;
	initialZ = transform.position.z;
	//bgHeight = GetComponent.<Renderer>().bounds.max.y - GetComponent.<Renderer>().bounds.min.y;
	//bgHeight = GameObject.Find("bgSolid1").GetComponent.<Renderer>().bounds.max.y - GameObject.Find("bgSolid1").GetComponent.<Renderer>().bounds.min.y;
	bgHeight = GetComponent.<Renderer>().bounds.max.y - GetComponent.<Renderer>().bounds.min.y;
	
	//Debug.Log(gameObject.name+" - boundsMaxY="+GetComponent.<Renderer>().bounds.max.y+" boundsMinY="+GetComponent.<Renderer>().bounds.min.y);
	//bgHeight = GetComponent.<Renderer>().bounds.size.y;
	//Debug.Log(gameObject.name+" - center="+GetComponent.<Renderer>().bounds.center+" - extents="+GetComponent.<Renderer>().bounds.extents);
	
	if(!isModeSeven){
		//Debug.Log("solid bgHeight="+bgHeight);
		GetComponent.<Rigidbody2D>().velocity.y = scrollSpeed;
	}
	else{
		//GetComponent.<Rigidbody>().velocity.x = 0;
		//GetComponent.<Rigidbody>().velocity.y = scrollSpeed;
		//GetComponent.<Rigidbody>().velocity.z = scrollSpeed;
		//Debug.Log("trans bgheight="+bgHeight);
		gameObject.transform.localScale.x *= 3;
		initialX = transform.position.x;
	}
}

function Update () {
	
	if(!isModeSeven){
		if(transform.position.y <= (initialY-bgHeight)){
			transform.position.y = initialY;
		}
	}
	
	if(isModeSeven){
		//transform.position += Vector3(0,0,scrollSpeed)*Time.deltaTime;
		var target = (Vector3.down*scrollSpeed*-1) * Time.deltaTime;
		transform.Translate(target);
		//Debug.Log(target.y);
		dist += target.y;
		//Debug.Log(dist);
		if(dist <= (-bgHeight)){
			//transform.position.z = initialZ;
			//Debug.Log(dist);
			var reset = Vector3(initialX,initialY,initialZ);
			transform.position = reset;
			dist=0;
			//transform.position = Vector3(initialX,initialY,initialZ);
		}
	}
}

function speedUp(){
	scrollSpeed-=0.25f;
	GetComponent.<Rigidbody2D>().velocity.y = scrollSpeed;
}

function setSpeed(newSpeed:double){
	if(newSpeed>0){newSpeed*=-1;}
	scrollSpeed=newSpeed;
	GetComponent.<Rigidbody2D>().velocity.y = scrollSpeed;
}