﻿
public var bullet : GameObject;
public var effectRing : GameObject;
public var delay : float = 0f;
public var speed : float = 5f;
public var wepNum : int = 1;
private var wepModifier : float=1f;

public var score : int = 0;
private var highScore : int = 0;
private var playerMoney : int = 0;
public var currHealth : int = 0;
public var maxHealth = 0f;

private var wepXp : int = 0;
private var shotTextures : Sprite[];
private var shipTextures : Sprite[];
private var shotsBuffer : GameObject[] = new GameObject[10];
private var shotsPositionBuffer : Vector3[] = new Vector3[10];
private var timer : int;

private var isControl:boolean=true;
private var isStageComplete:boolean=false;

private var isInvincible:boolean=false;
private var invincibleTime:float=0f;
private var isBlinking:boolean=false;
private var isHyper:boolean=false;
private var hyperTime:float=0f;
private var origWepNum:int=0;

public var usedBomb:boolean=false;
public var charOverride:String="chinchilla";

private var numBombs:int=3;

private var gameControllerObject : GameObject ;
gameControllerObject= GameObject.FindWithTag ("GameController");

function Start() {
	

	highScore = PlayerPrefs.GetInt("highScore",0);
	playerMoney = PlayerPrefs.GetInt("playerMoney",0);
	
	numBombs = PlayerPrefs.GetInt("numBombs",3);
	if(numBombs>7){numBombs=7;}
	wepNum = PlayerPrefs.GetInt("wepNum",1);
	if(wepNum>6){wepNum=6;}
	
	currHealth=3;
	maxHealth=3;
			
	GameObject.Find("highScore").GetComponent(UI.Text).text = "High Score : "+highScore;
	GameObject.Find("ScoreText").GetComponent(UI.Text).text = "Score : "+score;
	GameObject.Find("currentMoney").GetComponent(UI.Text).text = "Money : "+playerMoney;
	
	timer=0;
	shotTextures = Resources.LoadAll.<Sprite>("shots");
	
	shipTextures = Resources.LoadAll.<Sprite>("playerShips");
	var charName:String;
	if(charOverride==""){
		charName = PlayerPrefs.GetString("character","carrot");
	}
	else{
		charName = charOverride;
	}
	Debug.Log(charName);
	loadPlayer(charName);
	
	updateGUIBombs();
}

function loadPlayer(charName:String){
	Debug.Log("Load player "+charName);
	var shipName = charName + "Ship";
	var shipImg = Resources.Load.<Sprite>("playerShips/"+shipName);
	var focusName = charName + "ShipFocus";
	var focusImg = Resources.Load.<Sprite>("playerShips/"+focusName);
	shipTextures = [shipImg,focusImg];
	GetComponent(SpriteRenderer).sprite = shipTextures[0];
	loadStats(charName);
}

function loadStats(charName:String){
	if(charName=="tyr"){
		delay = 0.2f;
		speed = 3f;
		maxHealth = 3f; currHealth = 3f;
		numBombs = 3;
		wepModifier = 1f;
	}
	else if(charName=="carrot"){
		delay = 0.1f;
		speed = 5f;
		maxHealth = 5f; currHealth = 5f;
		numBombs = 5;
		wepModifier = 0.5f;
	}
	else if(charName=="beetle"){
		delay = 0.15f;
		speed = 4f;
		maxHealth = 6f; currHealth = 6f;
		numBombs = 5;
		wepModifier = 0.5f;
	}
	else if(charName=="chinchilla"){
		delay = 0.15f;
		speed = 4f;
		maxHealth = 6f; currHealth = 6f;
		numBombs = 5;
		wepModifier = 0.5f;
	}
}

function Update () {
	
	timer+=1;
	if(timer==60){timer=0;}
	
	if(isControl){
		if(Time.timeScale != 0){
			if(Input.GetKey("left") && transform.position.x > -0.36){
				transform.position.x -= speed*Time.deltaTime;
			}
			if(Input.GetKey("right") && transform.position.x < 4.33){
				transform.position.x += speed*Time.deltaTime;
			}
			if(Input.GetKey("up") && transform.position.y < 2){
				transform.position.y += speed*Time.deltaTime;
			}
			if(Input.GetKey("down") && transform.position.y > -1.98){
				transform.position.y -= speed*Time.deltaTime;
			}
		}
		
		if (Input.GetKeyDown("left shift")){
			speed = speed/2;
			GetComponent(SpriteRenderer).sprite = shipTextures[1];
		}
		if (Input.GetKeyUp("left shift")){
			speed = speed*2;
			GetComponent(SpriteRenderer).sprite = shipTextures[0];
		}
		
		if (Input.GetKeyDown("z")){
			InvokeRepeating("Shoot",0f,delay);
		}
		if (Input.GetKeyUp("z")){
			CancelInvoke("Shoot");
		}
		
		if((Input.GetKey("z"))&&(Time.timeScale==0)){
			Time.timeScale=1;
			//Application.LoadLevel("MainScene");
			Application.LoadLevel("chinchillaUniverse");
		}
		
		/*else if ((Input.GetKey("z")||Input.GetMouseButton(0)) && (timer%delay)==0) {
			Shoot();
		}   
	   
		if (Input.GetKeyUp ("z")){
			timer = 0;
		}*/
		
		if (Input.GetKeyDown("x")){
			if(numBombs>0){
				throwBomb();
			}
		}
	   
	   
		if (Input.GetKey("1")) {wepNum=1;}
		if (Input.GetKey("2")) {wepNum=2;}
		if (Input.GetKey("3")) {wepNum=3;}
		if (Input.GetKey("4")) {wepNum=4;}
		if (Input.GetKey("5")) {wepNum=5;}
		if (Input.GetKey("6")) {wepNum=6;}
		if (Input.GetKey("7")) {wepNum=7;}
		if (Input.GetKey("8")) {wepNum=8;}
		if (Input.GetKey("9")) {wepNum=9;}
		if (Input.GetKey("0")) {wepNum=10;}
	}
	
	if(isHyper){
		if(hyperTime>=0){
			gameObject.GetComponent.<Renderer>().material.color = new Color(Random.Range(0.1f,0.9f),Random.Range(0.1f,0.9f),Random.Range(0.1f,0.9f),1);
			hyperTime -= Time.deltaTime*1;
		}
		else{
			
			endHyper();
		}
	}
	
	if(isInvincible){
		if(invincibleTime>=0){
			invincibleTime -= Time.deltaTime*1;
		}
		else{
			endInvincible();
		}
	}
	
	if(isStageComplete){
		transform.position.y += speed*Time.deltaTime*2.5;
		//GameObject.Find("BGM").volume -= 20*Time.deltaTime;
	}
}

function updateGUIBombs(){
	for(var ii=1;ii<=7;ii++){
		var bombName = "guiBomb"+ii;
		var bombObj = GameObject.Find(bombName).GetComponent.<CanvasRenderer>();
		bombObj.SetColor(new Color(0,0,0,1));
		if(numBombs>=ii){bombObj.SetColor(new Color(1,1,1,1));}
	}
}

function takeDmg(howMuch:int){
	if(!isInvincible){
		GameObject.FindWithTag("GameController")/*.GetComponent.<MonoScript>()*/.SendMessage("blowUp",gameObject);
		currHealth-=howMuch;
		if(currHealth<=0){
			endLogic();
		}
		else{
			goInvincible(2f);
		}
	}
}

function goInvincible(howLong:float){
	isInvincible=true;
	invincibleTime=howLong;
	InvokeRepeating("blink",0f,0.05f);
}

function endInvincible(){
	gameObject.GetComponent.<Renderer>().enabled = true;
	isInvincible=false;
	invincibleTime=0f;
	CancelInvoke("blink");
}

function blink(){
	var rendObj = gameObject.GetComponent.<Renderer>();
	if(rendObj.enabled){
		rendObj.enabled=false;
	}
	else{
		rendObj.enabled=true;
	}
}

function goHyper(howLong:float){
	isHyper=true;
	hyperTime=howLong;
	origWepNum = wepNum;
	wepNum+=4; if(wepNum>6){wepNum=6;}
}

function endHyper(){
	gameObject.GetComponent.<Renderer>().material.color = new Color(1,1,1,1);
	isHyper=false;
	hyperTime=0f;
	wepNum = origWepNum;
}

function levelComplete(){

	CancelInvoke("Shoot");
	isInvincible=true;
	isControl=false;
	yield WaitForSeconds(1f);
	isStageComplete=true;
	InvokeRepeating("createAfterImages",0f,0.05f);
	GameObject.Find("goodJingle").GetComponent.<AudioSource>().Play();
	GameObject.Find("currentMoney").GetComponent(UI.Text).text = "Money : "+playerMoney+"  +"+score;
	
	saveData();
	
	yield WaitForSeconds(3f);
	Application.LoadLevel("chinchillaUniverse");
}

function createAfterImages(){
	var temp:GameObject = Instantiate(gameObject,transform.position,Quaternion.identity);
	temp.GetComponent(spaceshipScript).SendMessage("setAsAfterImage");
}

function setAsAfterImage(){
	isControl=false;
	gameObject.GetComponent(fadeScript).enabled = true;
	Destroy(gameObject,1f);
}

function throwBomb(){
	usedBomb=true;
	numBombs--;
	updateGUIBombs();
	for(var ii=0;ii<15;ii++){
		
		var randPos = GameObject.Find("playCenter").transform.position;
		randPos.x += Random.Range(-1.5f,1.5f);
		randPos.y += Random.Range(-1.5f,1.5f);
		var ring = Instantiate(effectRing,randPos,Quaternion.identity);
		ring.GetComponent("lerpScale").SendMessage("setMode","grow");
		yield WaitForSeconds(0.01f);
	}
}

function OnParticleCollision (other : GameObject) {
	Debug.Log("particle collision!");
	GameObject.FindWithTag("GameController")/*.GetComponent.<MonoScript>()*/.SendMessage("blowUp",other);
	takeDmg(1);
}

function Shoot (){

	
	gameControllerObject/*.GetComponent("levelOneScript")*/.SendMessage("pewpew");

    var leftB = transform.position; var rightB = transform.position; 
    var frontB = transform.position; var backB = transform.position;
	
    leftB.x -= 0.2; rightB.x += 0.2; 
    frontB.y += 0.2; backB.y -= 0.2;
	
   	
    switch (wepNum) {
    	case 1:
    		shotsPositionBuffer[0] = frontB; shotsPositionBuffer[0].x -= 0.05*wepModifier;
    		shotsBuffer[0] = Instantiate(bullet, shotsPositionBuffer[0], Quaternion.identity);
    		shotsBuffer[0].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		shotsPositionBuffer[1] = frontB; shotsPositionBuffer[1].x += 0.05*wepModifier;
    		shotsBuffer[1] = Instantiate(bullet, shotsPositionBuffer[1], Quaternion.identity);
    		shotsBuffer[1].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		break;
    		
    	case 2:
    		shotsPositionBuffer[0] = frontB; shotsPositionBuffer[0].x -= 0.1*wepModifier;
    		shotsBuffer[0] = Instantiate(bullet, shotsPositionBuffer[0], Quaternion.identity);
    		shotsBuffer[0].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		shotsPositionBuffer[1] = frontB; shotsPositionBuffer[1].x += 0.1*wepModifier;
    		shotsBuffer[1] = Instantiate(bullet, shotsPositionBuffer[1], Quaternion.identity);
    		shotsBuffer[1].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		shotsPositionBuffer[2] = frontB; shotsPositionBuffer[2].y += 0.1*wepModifier;
    		shotsBuffer[2] = Instantiate(bullet, shotsPositionBuffer[2], Quaternion.identity);
    		shotsBuffer[2].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		break;
    		
    	case 3:
    		shotsPositionBuffer[0] = leftB; shotsPositionBuffer[0].x += 0.1*wepModifier;
    		shotsBuffer[0] = Instantiate(bullet, shotsPositionBuffer[0], Quaternion.identity);
    		shotsBuffer[0].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		shotsPositionBuffer[1] = frontB; shotsPositionBuffer[1].x -= 0.035*wepModifier;
    		shotsBuffer[1] = Instantiate(bullet, shotsPositionBuffer[1], Quaternion.identity);
    		shotsBuffer[1].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		shotsPositionBuffer[2] = frontB; shotsPositionBuffer[2].x += 0.035*wepModifier;
    		shotsBuffer[2] = Instantiate(bullet, shotsPositionBuffer[2], Quaternion.identity);
    		shotsBuffer[2].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		shotsPositionBuffer[3] = rightB; shotsPositionBuffer[3].x -= 0.1*wepModifier;
    		shotsBuffer[3] = Instantiate(bullet, shotsPositionBuffer[3], Quaternion.identity);
    		shotsBuffer[3].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		break;
    		
    	case 4:
    		shotsPositionBuffer[0] = frontB; shotsPositionBuffer[0].x -= 0.2*wepModifier;
    		shotsBuffer[0] = Instantiate(bullet, shotsPositionBuffer[0], Quaternion.identity);
    		shotsBuffer[0].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		shotsPositionBuffer[1] = frontB; shotsPositionBuffer[1].x += 0.2*wepModifier;
    		shotsBuffer[1] = Instantiate(bullet, shotsPositionBuffer[1], Quaternion.identity);
    		shotsBuffer[1].GetComponent(SpriteRenderer).sprite = shotTextures[0];
    		
    		shotsPositionBuffer[2] = frontB; shotsPositionBuffer[2].y += 0.1*wepModifier;
    		shotsBuffer[2] = Instantiate(bullet, shotsPositionBuffer[2], Quaternion.identity);
    		shotsBuffer[2].GetComponent(SpriteRenderer).sprite = shotTextures[1];
    		break;
    		
    	case 5:    		
    		shotsPositionBuffer[2] = frontB; shotsPositionBuffer[2].x -= 0.2*wepModifier;
    		shotsBuffer[2] = Instantiate(bullet, shotsPositionBuffer[2], Quaternion.identity);
    		shotsBuffer[2].GetComponent(SpriteRenderer).sprite = shotTextures[2];
    		shotsBuffer[2].GetComponent("bulletScript").SendMessage("toAngle", 105f);
    		shotsBuffer[2].GetComponent("bulletScript").SendMessage("setSpeed", 0.5);
    		
    		shotsPositionBuffer[3] = frontB; shotsPositionBuffer[3].x += 0.2*wepModifier;
    		shotsBuffer[3] = Instantiate(bullet, shotsPositionBuffer[3], Quaternion.identity);
    		shotsBuffer[3].GetComponent(SpriteRenderer).sprite = shotTextures[2];
    		shotsBuffer[3].GetComponent("bulletScript").SendMessage("toAngle", 75f);
    		shotsBuffer[3].GetComponent("bulletScript").SendMessage("setSpeed", 0.5);
    		
    		shotsPositionBuffer[4] = frontB; shotsPositionBuffer[4].y += 0.1*wepModifier;
    		shotsBuffer[4] = Instantiate(bullet, shotsPositionBuffer[4], Quaternion.identity);
    		shotsBuffer[4].GetComponent(SpriteRenderer).sprite = shotTextures[1];
    		break;
    		
    	case 6:
    		shotsPositionBuffer[0] = frontB; shotsPositionBuffer[0].x -= 0.2*wepModifier;
    		shotsBuffer[0] = Instantiate(bullet, shotsPositionBuffer[0], Quaternion.identity);
    		shotsBuffer[0].GetComponent(SpriteRenderer).sprite = shotTextures[2];
    		shotsBuffer[0].GetComponent("bulletScript").SendMessage("toAngle", 135f);
    		shotsBuffer[0].GetComponent("bulletScript").SendMessage("setSpeed", 0.5);
    		
    		shotsPositionBuffer[1] = frontB; shotsPositionBuffer[1].x += 0.2*wepModifier;
    		shotsBuffer[1] = Instantiate(bullet, shotsPositionBuffer[1], Quaternion.identity);
    		shotsBuffer[1].GetComponent(SpriteRenderer).sprite = shotTextures[2];
    		shotsBuffer[1].GetComponent("bulletScript").SendMessage("toAngle", 45f);
    		shotsBuffer[1].GetComponent("bulletScript").SendMessage("setSpeed", 0.5);
    		
    		shotsPositionBuffer[2] = frontB; shotsPositionBuffer[2].x -= 0.2*wepModifier;
    		shotsBuffer[2] = Instantiate(bullet, shotsPositionBuffer[2], Quaternion.identity);
    		shotsBuffer[2].GetComponent(SpriteRenderer).sprite = shotTextures[2];
    		shotsBuffer[2].GetComponent("bulletScript").SendMessage("toAngle", 105f);
    		shotsBuffer[2].GetComponent("bulletScript").SendMessage("setSpeed", 0.5);
    		
    		shotsPositionBuffer[3] = frontB; shotsPositionBuffer[3].x += 0.2*wepModifier;
    		shotsBuffer[3] = Instantiate(bullet, shotsPositionBuffer[3], Quaternion.identity);
    		shotsBuffer[3].GetComponent(SpriteRenderer).sprite = shotTextures[2];
    		shotsBuffer[3].GetComponent("bulletScript").SendMessage("toAngle", 75f);
    		shotsBuffer[3].GetComponent("bulletScript").SendMessage("setSpeed", 0.5);
    		
    		shotsPositionBuffer[4] = frontB; shotsPositionBuffer[4].y += 0.1*wepModifier; shotsPositionBuffer[4].x += 0.1*wepModifier;
    		shotsBuffer[4] = Instantiate(bullet, shotsPositionBuffer[4], Quaternion.identity);
    		shotsBuffer[4].GetComponent(SpriteRenderer).sprite = shotTextures[3];
    		
    		shotsPositionBuffer[5] = frontB; shotsPositionBuffer[5].y += 0.1*wepModifier; shotsPositionBuffer[5].x -= 0.1*wepModifier;
    		shotsBuffer[5] = Instantiate(bullet, shotsPositionBuffer[5], Quaternion.identity);
    		shotsBuffer[5].GetComponent(SpriteRenderer).sprite = shotTextures[3];
    		break;
    }
}

function endLogic(){
	
	GameObject.Find("BGM").GetComponent.<AudioSource>().Stop();
	GameObject.Find("playerDeath").GetComponent.<AudioSource>().Play();

	GameObject.Find("currentMoney").GetComponent(UI.Text).text = "Money : "+playerMoney+"  +"+score;
	saveData();
	gameObject.GetComponent.<Renderer>().material.color = Color.red;
    Time.timeScale = 0; 
}

function saveData(){
	var profit = playerMoney + score;
	PlayerPrefs.SetInt("playerMoney", profit);
	PlayerPrefs.SetInt("numBombs", numBombs);
	PlayerPrefs.SetInt("wepNum", wepNum);
	if(score>highScore){
		PlayerPrefs.SetInt("highScore", score);
	}
	PlayerPrefs.Save();
}