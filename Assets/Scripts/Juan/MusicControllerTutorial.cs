﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class MusicControllerTutorial : MonoBehaviour {


	public AudioMixerSnapshot Space;
	public AudioMixerSnapshot Action;
	public AudioClip[] stings;
	public AudioSource stingSource;
	public float bpm = 140;

	private float m_TransitionIn;
	private float m_TransitionOut;
	private float m_quarterNote;
	// Use this for initialization
	void Start () {
	
		m_quarterNote = 60 / bpm;
		m_TransitionIn = m_quarterNote * 2;
		m_TransitionOut = m_quarterNote * 4;

	}
	
	void OnTriggerEnter2D(Collider2D other){

		if (other.CompareTag ("enemy")) {
			Action.TransitionTo(m_TransitionIn);
		}

	}

	void OnTriggerExit2D(Collider2D other){
		if (other.CompareTag ("enemy")) {
			Space.TransitionTo(m_TransitionOut);
		}

	}

}
