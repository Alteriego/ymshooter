﻿#pragma strict

public var DampTime = 0.5f;
public var target : Transform;
private var velocity = Vector3.zero;
private var delta : Vector3;


function FixedUpdate () {

	var point = GetComponent.<Camera>().WorldToScreenPoint(target.position);
	delta = target.position - GetComponent.<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
	var destination : Vector3 = transform.position + delta;
	transform.position = Vector3.SmoothDamp(transform.position, destination, velocity, DampTime);

}