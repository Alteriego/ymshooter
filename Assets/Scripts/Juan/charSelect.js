﻿#pragma strict

public var charOverride:String="";
private var playerSprite : Sprite[];

function Start () {

var charName:String;
	if(charOverride==""){
		charName = PlayerPrefs.GetString("character","chinchilla");
	}
	else{
		charName = charOverride;
	}
	Debug.Log(charName);
	loadPlayerSprite(charName);


}

function loadPlayerSprite(charName:String){
	Debug.Log("Load player "+charName);
	var shipName = charName + "Ship";
	var shipImg = Resources.Load.<Sprite>("playerShips/"+shipName);
	
	playerSprite = [shipImg];
	GetComponent.<SpriteRenderer>().sprite = playerSprite[0];
	
	if(charName == "chinchilla"){
		GetComponent.<Animator>().enabled = true;
	}
	//loadStats(charName);
}