﻿#pragma strict


var rb : Rigidbody2D;
var setRot : Transform;
public var flySpeed= 5f;
public var walkSpeed = 5f;
var angle : float;
private var onGround : boolean;
var jetPack : GameObject;
//load position vars
var posX : float;
var posY : float;
var playerRot : float;
var transformOverride : boolean;


function Start () {
rb = GetComponent.<Rigidbody2D>();
setRot = GetComponent.<Transform>();
jetPack = GameObject.Find("Solar Plasma JetPack");

//Load Transform Position
		if(transformOverride == false){
		posX = PlayerPrefs.GetFloat("playerPositionX");
		posY = PlayerPrefs.GetFloat("playerPositionY");
		playerRot = PlayerPrefs.GetFloat("playerRotation");
		transform.position = Vector3(posX, posY, transform.position.z);
		angle = playerRot;
		

	}
}

function savePosition(){
	var getPlayerPos : Vector3;
	var object : GameObject;
	object = GameObject.Find("Player");
	getPlayerPos = object.transform.position;
	var getPlayerRot = object.transform.localEulerAngles;
	print(getPlayerRot);

	PlayerPrefs.SetFloat("playerPositionX", getPlayerPos.x);
	PlayerPrefs.SetFloat("playerPositionY", getPlayerPos.y);
	PlayerPrefs.SetFloat("playerRotation", getPlayerRot.z);

}

function FixedUpdate () {
    
    if(Input.GetKeyDown(KeyCode.Return)){
    	savePosition();
    }
    if(onGround == false){
    rb.isKinematic = false;
    movementFly();
   } else {
   
   	rb.isKinematic = true;
    movementWalk();
   }
}

function movementFly(){
    
    var moveH = Input.GetAxisRaw("Horizontal");
    var moveV = Input.GetAxisRaw("Vertical");
    
    if (moveH > 0 && moveV > 0) {
        rb.AddForce(Vector2.one * flySpeed);
        angle = -45f;
        jetpackBoost();
        
    } else if (moveH < 0 && moveV < 0) {
        rb.AddForce(Vector2.one * -flySpeed);
        angle = 135f;
        jetpackBoost();
    } else if (moveH > 0 && moveV < 0) {
        rb.AddForce(Vector2(1,-1) * flySpeed);
        angle = -135f;
        jetpackBoost();
        
    } else if (moveH < 0 && moveV > 0) {
        rb.AddForce(Vector2(-1,1) * flySpeed);
        angle = 45;
        jetpackBoost();
        
    }else if (moveH > 0) {
        rb.AddForce(Vector2.right * flySpeed);
        angle = 270f;
        jetpackBoost();
        
    }else if (moveH < 0){
        rb.AddForce(Vector2.right * -flySpeed);
        angle = 90f;
        jetpackBoost();
        
    }
    
    else if (moveV > 0) {
        rb.AddForce(Vector2.up * flySpeed);
        angle = 0f;
        jetpackBoost();
        
    }else if (moveV < 0) {
        rb.AddForce(Vector2.up * -flySpeed);
        angle = 180f;
        jetpackBoost();
        
    } else {
    	
    	jetPackOff();
    }
    
   
   
    setRot.localEulerAngles = Vector3(0,0,angle);
    
    
}

function jetpackBoost(){
	
	jetPack.GetComponent.<ParticleSystem>().enableEmission = true;
	
}

function jetPackOff(){
jetPack.GetComponent.<ParticleSystem>().enableEmission = false;
}



function movementWalk(){


	var moveH = Input.GetAxisRaw("Horizontal");
    var moveV = Input.GetAxisRaw("Vertical");
    
    if (moveH > 0 && moveV > 0) {
        transform.position.x += walkSpeed * Time.deltaTime;
        transform.position.y += walkSpeed * Time.deltaTime;
        angle = -45f;
        
    } else if (moveH < 0 && moveV < 0) {
        transform.position.x += -walkSpeed * Time.deltaTime;
        transform.position.y += -walkSpeed * Time.deltaTime;
        angle = 135f;
        
    } else if (moveH > 0 && moveV < 0) {
        transform.position.x += walkSpeed * Time.deltaTime;
        transform.position.y += -walkSpeed * Time.deltaTime;
        angle = -135f;
        
    } else if (moveH < 0 && moveV > 0) {
        transform.position.x += -walkSpeed * Time.deltaTime;
        transform.position.y += walkSpeed * Time.deltaTime;
        angle = 45;
        
    }else if (moveH > 0) {
        transform.position.x += walkSpeed * Time.deltaTime;
        angle = 270f;
    }else if (moveH < 0){
        transform.position.x += -walkSpeed * Time.deltaTime;
        angle = 90f;
    }else if (moveV > 0) {
        transform.position.y += walkSpeed * Time.deltaTime;
        angle = 0f;
    }else if (moveV < 0) {
        transform.position.y += -walkSpeed * Time.deltaTime;
        angle = 180f;
    }
    
    
    
    
    setRot.localEulerAngles = Vector3(0,0,angle);
}

function OnTriggerStay2D(other: Collider2D){
	


	if(other.CompareTag("space")){
		
		onGround = false;
		
		
	}else if(other.CompareTag("ground")){
	onGround = true;
	jetPackOff();
	}

}
//function OnTriggerExit2D(other: Collider2D){
//
//	
//
//	if(other.CompareTag("ground")){
//		
//		onGround = false;
//		
//	}
//
//}

	
	

