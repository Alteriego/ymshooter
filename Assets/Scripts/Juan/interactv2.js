﻿#pragma strict


public var DialogBox : GameObject;
public var InstructionsText : GameObject;
private var findCanvas : GameObject;
private var findCanvasTransform : Transform;
public var textvar : UI.Text;
public var DialogText = "";
public var LoadsLevel : boolean;
public var levelToLoad = 0;
public var loadDelay = 1f;
private var playerObject : GameObject;



function Start () {
	
	findCanvas = GameObject.Find("Canvas");
	findCanvasTransform = findCanvas.GetComponent.<Transform>();
	

}

function OnTriggerEnter2D (other : Collider2D){

	print ("Waffle Collision");
	
	instructionFunction();
	
}

function OnTriggerStay2D (other : Collider2D){

	if(Input.GetKeyDown(KeyCode.E)){
		
		openDialogBox();
		if(LoadsLevel == true){
		
		savePosition();
		loadLevel();
		}
	}
}

function OnTriggerExit2D (other : Collider2D){

	InstructionsText.SetActive(false);
	DialogBox.SetActive(false);

}



function instructionFunction(){
	InstructionsText.SetActive(true);
}

function openDialogBox(){

	//Open the dialog Box
	InstructionsText.SetActive(false);
	DialogBox.SetActive(true);
	DialogContent();

}

function DialogContent(){
	
	textvar.text = DialogText;
	
}

function loadLevel(){
	
	yield WaitForSeconds(loadDelay);
	Application.LoadLevel(levelToLoad);
	
}

function savePosition(){
	var getPlayerPos : Vector3;

	playerObject = GameObject.Find("Player");
	getPlayerPos = playerObject.transform.position;
	print(getPlayerPos);

	PlayerPrefs.SetFloat("playerPositionX", getPlayerPos.x);
	PlayerPrefs.SetFloat("playerPositionY", getPlayerPos.y);

}
