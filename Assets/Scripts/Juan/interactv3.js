﻿#pragma strict


public var DialogBox : GameObject;
public var InstructionsText : GameObject;
private var findCanvas : GameObject;
private var findCanvasTransform : Transform;
public var dialogContent : UI.Text; //changed from textvar for sake of knowing what to put where in editor
public var DialogText = "";
public var LoadsLevel : boolean;
public var loadsMenu : boolean;
public var levelToLoad = 0;
public var loadDelay = 1f;
private var playerObject : GameObject;
public var shopMenu : GameObject;



private var insTrue : boolean;


function Start () {
	
	findCanvas = GameObject.Find("Canvas");
	findCanvasTransform = findCanvas.GetComponent.<Transform>();
	

}

//function OnTriggerEnter2D (other : Collider2D){
//
//	print ("Waffle Collision");
//	
//	instructionFunction();
//	
//}

function OnTriggerStay2D (other : Collider2D){
	
	if(other.CompareTag("Player")){
		InstructionsText.SetActive(true);
	
		if(Input.GetKeyDown(KeyCode.E)){
			savePosition();
			openDialogBox();
		
			if(LoadsLevel == true){
			loadLevel();
			}else if(loadsMenu == true){
			loadMenu();
			}
		}
	}
}


function OnTriggerExit2D (other : Collider2D){

	InstructionsText.SetActive(false);
	DialogBox.SetActive(false);

}



function openDialogBox(){

	//Open the dialog Box
	InstructionsText.SetActive(false);
	DialogBox.SetActive(true);
	DialogContent();

}

function DialogContent(){
	
	dialogContent.text = DialogText;
	
}

function loadLevel(){
	
	yield WaitForSeconds(loadDelay);
	Application.LoadLevel(levelToLoad);
	
}

function loadMenu(){
	yield WaitForSeconds(loadDelay);
	shopMenu.SetActive (!shopMenu.activeSelf);
	
	
}

function savePosition(){
	var getPlayerPos : Vector3;

	playerObject = GameObject.Find("Player");
	getPlayerPos = playerObject.transform.position;
	print(getPlayerPos);

	PlayerPrefs.SetFloat("playerPositionX", getPlayerPos.x);
	PlayerPrefs.SetFloat("playerPositionY", getPlayerPos.y);

}
