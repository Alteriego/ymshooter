﻿#pragma strict

public var rotationSpeed = Vector3(0f, 5f, 0f);


function Update () {
	
	
	transform.Rotate((Vector3.up + rotationSpeed) * Time.deltaTime);

}