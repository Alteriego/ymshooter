﻿using UnityEngine;
using System.Collections;

public class upDownScript : MonoBehaviour {

	Vector3 startPos;
	public float vAmplitude = 3f;
	public float vPeriod = 3f;
	public float hAmplitude = 3f;
	public float hPeriod = 3f;
	public bool randomizeH = false;

	// Use this for initialization
	void Start () {
		startPos = transform.position;
		randomizeH = false;
	}
	
	// Update is called once per frame
	void Update () {
		float theta = Time.timeSinceLevelLoad / vPeriod;
		float distance = vAmplitude * Mathf.Sin (theta);

//		if (randomizeH = true){
//			hAmplitude = Mathf.PingPong(Time.time, 5);
//		}


		//print (transform.position);
		float hTheta = Time.timeSinceLevelLoad / hPeriod;
		float hDistance = hAmplitude * Mathf.Sin (hTheta);
		transform.position = startPos + Vector3.right * hDistance + Vector3.up * distance;
//		transform.position = Vector3(startPos.x + Vector3.right * hDistance, startPos.y + Vector3.up * distance);
	}
}
