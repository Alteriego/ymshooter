﻿#pragma strict

var notifyImage : GameObject;
var notifyText : UnityEngine.UI.Text;
var notificationTimer : float;

function Start()
{
	notificationTimer = 0;
}

function Update()
{
	Debug.Log(notificationTimer);
	notificationTimer -= Time.unscaledDeltaTime;
	if (notificationTimer > 0){notifyImage.SetActive(true);} 
	else {notifyImage.SetActive(false);}
}

function notifySwitch ()
{
	notificationTimer = 1;
	notifyImage.SetActive(!notifyImage.activeSelf);
	notifyText.GetComponent(UI.Text).text = "Mode Switched!";
}

function SeriousMode ()
{
	PlayerPrefs.SetInt("currentHUD", 3);
	notifySwitch();
}

function HGBTMode ()
{
	PlayerPrefs.SetInt("currentHUD", 2);
	notifySwitch();
}

function OtakuMode ()
{
	PlayerPrefs.SetInt("currentHUD", 1);
	notifySwitch();
}

function BoringMode ()
{
	PlayerPrefs.SetInt("currentHUD", 0);
	notifySwitch();	
}