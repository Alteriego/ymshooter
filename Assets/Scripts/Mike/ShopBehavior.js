﻿#pragma strict

private var playerMoney : int;
var moneyText : UnityEngine.UI.Text;
var upgradeWeaponButton : UnityEngine.UI.Button;
var buyBombButton : UnityEngine.UI.Button;
var upgradeWeaponPrice : int = 100;
var buyBombButtonPrice : int = 50;
private var numBombs = 0;
private var wepNum = 0;
private var cheatMoney : int = 1000;


function Start ()
{
	numBombs = PlayerPrefs.GetInt("numBombs",3);
	if(numBombs>7){numBombs=7;}
	
	wepNum = PlayerPrefs.GetInt("wepNum",1);
	if(wepNum>6){wepNum=6;}
	
	playerMoney = PlayerPrefs.GetInt("playerMoney",0);
	Debug.Log(playerMoney);
	GameObject.Find("PlayerMoney").GetComponent(UI.Text).text = "Money: $"+playerMoney;
	//buyBombButton.interactable=false;
	//upgradeWeaponButton.interactable=false;
	updateButtons();
	updateMoney ();
	updateGUIUpgrades();
	updateGUIBombs();
}

function updateButtons ()
{
	buyBombButton.interactable = false;
	upgradeWeaponButton.interactable = false;
	
	if (playerMoney >= 50 && numBombs <7){
		//Debug.Log("gr8r than 100");
		buyBombButton.interactable = true;
		}
	if (playerMoney >= 100 && wepNum <6){
		//Debug.Log("gr8r than 100");
		upgradeWeaponButton.interactable = true;
		}
	/*
	else {
		buyBombButton.interactable = true;
		upgradeWeaponButton.interactable = true;
		}
	*/
}

function upgradeWeapon ()
{
	playerMoney -= upgradeWeaponPrice;
	wepNum++;
	saveMoney();
	GameObject.Find("PlayerMoney").GetComponent(UI.Text).text = "Moolah : "+playerMoney;
	Debug.Log("Weapon Upgraded!");
	updateButtons();
	updateMoney();
	updateGUIUpgrades();
	saveData();
}

function buyBombs ()
{
	playerMoney -= buyBombButtonPrice;
	numBombs++;
	saveMoney();
	GameObject.Find("PlayerMoney").GetComponent(UI.Text).text = "Moolah : "+playerMoney;
	Debug.Log("Bomb Get!");
	updateButtons();
	updateMoney();
	updateGUIBombs();
	saveData();
}

function saveMoney()
{
	PlayerPrefs.SetInt("playerMoney", playerMoney);
	PlayerPrefs.Save();
}

function updateMoney () 
{
	moneyText.GetComponent(UI.Text).text = "Money: $"+playerMoney;
}

function updateGUIBombs()
{
	for(var ii=1;ii<=7;ii++){
	var bombName = "guiBomb"+ii;
	var bombObj = GameObject.Find(bombName).GetComponent.<CanvasRenderer>();
	bombObj.SetColor(new Color(0,0,0,1));
	if(numBombs>=ii){bombObj.SetColor(new Color(1,1,1,1));}}
}

function updateGUIUpgrades()
{
	for(var ii=1;ii<=6;ii++){
	var upgradeName = "guiUpgrade"+ii;
	var upgradeObj = GameObject.Find(upgradeName).GetComponent.<CanvasRenderer>();
	upgradeObj.SetColor(new Color(0,0,0,1));
	if(wepNum>=ii){upgradeObj.SetColor(new Color(1,1,1,1));}}
}

function saveData()
{
	PlayerPrefs.SetInt("numBombs", numBombs);
	PlayerPrefs.SetInt("wepNum", wepNum);
	PlayerPrefs.Save();
}

function resetstats()
{
	numBombs = 3;
	wepNum = 1;
	updateGUIBombs();
	updateGUIUpgrades();
	saveData();	
	updateButtons ();
}	

function cheat ()
{
	playerMoney += cheatMoney;
	saveMoney();
	updateButtons ();
}
	