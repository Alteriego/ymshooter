﻿#pragma strict

var notifyImage : GameObject;
var notifyText : UnityEngine.UI.Text;
var notificationTimer : float;

function Start()
{
	notificationTimer = 0;
}

function Update()
{
	Debug.Log(notificationTimer);
	notificationTimer -= Time.unscaledDeltaTime;
	if (notificationTimer > 0){notifyImage.SetActive(true);} 
	else {notifyImage.SetActive(false);}
}

function notifySwitch ()
{
	notificationTimer = 1;
	notifyImage.SetActive(!notifyImage.activeSelf);
	notifyText.GetComponent(UI.Text).text = "Character Switched!";
}

function charSelectShip ()
{
	PlayerPrefs.SetString("character","tyr");
	notifySwitch ();
}

function charSelectCarrot ()
{
	PlayerPrefs.SetString("character","carrot");
	notifySwitch ();
}

function charSelectBeetle ()
{
	PlayerPrefs.SetString("character","beetle");
	notifySwitch ();
}
