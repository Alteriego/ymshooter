﻿#pragma strict

var shopMenu : GameObject;
var mainPanel : GameObject;
var panelCount : GameObject [];
var firstButtonMain : UnityEngine.UI.Button;
var firstButtonWeapons : UnityEngine.UI.Button;
var firstButonLooknFeel : UnityEngine.UI.Button;
var firstButtonCharacters : UnityEngine.UI.Button;

//The following is already on pausePanelBehavior
function Awake ()
{
	panelCount = GameObject.FindGameObjectsWithTag("menupanel");
	CloseAllPanels();
}

function Start () 
{
	mainPanel = GameObject.Find ("InitialPanel");
	shopMenu.SetActive(false);
}

function OnEnable()
{	
	mainPanel.SetActive (true);
	firstButtonMain.Select ();
}

// this function takes care of switching between panels
function togglePanel (panel : GameObject) 
{
	panel.SetActive (!panel.activeSelf);
	mainPanel.SetActive (!mainPanel.activeSelf);
}

// the following set the focus on their respective first button for panel
function openWeapons ()
{
	firstButtonWeapons.Select ();
	Debug.Log ("Selected panel is: " +firstButtonWeapons);
}

function openLooknFeel ()
{
	firstButonLooknFeel.Select ();
	Debug.Log ("Selected panel is: " +firstButonLooknFeel);
}

function openCharacters ()
{
	firstButtonCharacters.Select ();
	Debug.Log ("Selected panel is: " +firstButtonCharacters);
}

function backToInitial ()
{
	firstButtonMain.Select ();
}

function backToOverworld ()
{
	shopMenu.SetActive (!shopMenu.activeSelf);
	if (Time.timeScale == 0){
	Time.timeScale=1;}
}

function CloseAllPanels ()
{
	for (var panels : GameObject in panelCount)
	{ 
		panels.SetActive(false);
	} 	
}