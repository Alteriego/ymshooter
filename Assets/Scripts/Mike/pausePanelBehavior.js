﻿#pragma strict

var pauseMenu : GameObject;
var mainPanel : GameObject;
var panelCount : GameObject [];
var firstButtonMain : UnityEngine.UI.Button;
var firstButonLooknFeel : UnityEngine.UI.Button;
var firstButtonCharacters : UnityEngine.UI.Button;

function Awake ()
{
	panelCount = GameObject.FindGameObjectsWithTag("pausepanel");
	CloseAllPanels();
}

function Start () 
{
	mainPanel = GameObject.Find ("InitialPanel");
	pauseMenu.SetActive(false);
}

function OnEnable()
{
	mainPanel.SetActive (true);
	firstButtonMain.Select ();
}

// this function takes care of switching between panels
function togglePanel (panel : GameObject) 
{
	panel.SetActive (!panel.activeSelf);
	mainPanel.SetActive (!mainPanel.activeSelf);
}


// the following set the focus on their respective first button for panel
function openLooknFeel ()
{
	firstButonLooknFeel.Select ();
	Debug.Log ("Selected panel is: " +firstButonLooknFeel);
}

function openCharacters ()
{
	firstButtonCharacters.Select ();
	Debug.Log ("Selected panel is: " +firstButtonCharacters);
}

function backToInitial ()
{
	firstButtonMain.Select ();
}

function backToOverworld ()
{
	pauseMenu.SetActive (!pauseMenu.activeSelf);
	if (Time.timeScale == 0){
	Time.timeScale=1;}
}

//restart & back to menu functions here
function restartScene ()
{
	Application.LoadLevel(Application.loadedLevelName);
	pauseMenu.SetActive (!pauseMenu.activeSelf);
	if (Time.timeScale == 0){
	Time.timeScale=1;}
}

function CloseAllPanels ()
{
	for (var panels : GameObject in panelCount)
	{ 
		panels.SetActive(false);
	} 	
}