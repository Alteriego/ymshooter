﻿#pragma strict

var pauseGame : boolean;
var pauseMenu : GameObject;
var shopMenu : GameObject;
//add any other kind of menu here too if relevant.

function Awake ()
{
	MakeSureTimeScaleIs1();
	FindTheMenus();
}

function Update () 
{
	if(Input.GetKeyDown(KeyCode.Escape)){
	PauseTheGame(); // Routes the script to relevant function depending on whether or not other menu objects exist in the scene
	}
}			
																
// The brunt of this code is this pause function that checks for pause state and other menus when pause is pressed

function PauseTheGame () //function that pauses the game if it finds a shop object
{
	pauseGame =!pauseGame;
	
	if((pauseGame == true) && (shopMenu.activeSelf ==false))
	{
		Time.timeScale = 0;
		pauseGame = true;
		AudioListener.pause = true;
		pauseMenu.SetActive (true);
		//pauseMenu.FindGameObjectWithTag("panel").SetActive(false);
		//pauseMenu.GetComponent(mainPanel).SetActive(true);
		//pauseMenu.GetComponent(pausePanelBehavior).firstButtonMain.Select ();
	}
	
	if((pauseGame ==false) && (shopMenu.activeSelf ==false))
	{
		Time.timeScale = 1;
		pauseGame = false;
		AudioListener.pause = false;
		pauseMenu.SetActive (false);
	}	
	
	if((pauseGame == true) && (shopMenu.activeSelf == true))
	{
		shopMenu.SetActive (false);
		Time.timeScale = 1;
		pauseGame = false;
		AudioListener.pause = false;
		pauseMenu.SetActive (false);
	}	
}

// The following are just functions I use to make sure other components behave properly

function MakeSureTimeScaleIs1 ()
{
	pauseGame = false;
	Time.timeScale = 1;
	Debug.Log ("Time Scale = " +Time.timeScale);
}
		
function FindTheMenus()
{
	pauseMenu = GameObject.Find("pauseMenu");
	pauseMenu.SetActive(false);
	shopMenu = GameObject.Find("shopMenu");
	shopMenu.SetActive(false);
}