﻿#pragma strict

var HUDImage : UnityEngine.UI.Image;
var scoreText : UnityEngine.UI.Text;
var bossHealth : UnityEngine.UI.Text;
var playerHealth : UnityEngine.UI.Text;
var playerBombs : UnityEngine.UI.Text;
var highScore : UnityEngine.UI.Text;
var currentMoney : UnityEngine.UI.Text;


var hud1 : Sprite;
var hud2 : Sprite;
var hud3 : Sprite;
var hud4 : Sprite;

function Start () {
		
	if (PlayerPrefs.GetInt("currentHUD")==0){
	HUDImage.overrideSprite = hud1;
	scoreText.color = new Color(0,1,0,1);
	bossHealth.color = new Color(0,1,0,1);
	playerHealth.color = new Color(0,1,0,1);
	playerBombs.color = new Color(0,1,0,1);
	highScore.color = new Color(0,1,0,1);
	currentMoney.color = new Color(0,1,0,1);
	}

	if (PlayerPrefs.GetInt("currentHUD")==1){
	HUDImage.overrideSprite = hud2;
	scoreText.color = new Color(0,0,0,1);
	bossHealth.color = new Color(0,0,0,1);
	playerHealth.color = new Color(0,0,0,1);
	playerBombs.color = new Color(0,0,0,1);
	highScore.color = new Color(0,0,0,1);
	currentMoney.color = new Color(0,0,0,1);
	}

if (PlayerPrefs.GetInt("currentHUD")==2){
	HUDImage.overrideSprite = hud3;
	scoreText.color = new Color(0,0,0,1);
	bossHealth.color = new Color(0,0,0,1);
	playerHealth.color = new Color(0,0,0,1);
	playerBombs.color = new Color(0,0,0,1);
	highScore.color = new Color(0,0,0,1);
	currentMoney.color = new Color(0,0,0,1);
	}
	
if (PlayerPrefs.GetInt("currentHUD")==3){
	HUDImage.overrideSprite = hud4;
	scoreText.color = new Color(1, 0.92, 0.016, 1);
	bossHealth.color = new Color(1, 0.92, 0.016, 11);
	playerHealth.color = new Color(1, 0.92, 0.016, 11);
	playerBombs.color = new Color(1, 0.92, 0.016, 11);
	highScore.color = new Color(1, 0.92, 0.016, 11);
	currentMoney.color = new Color(1, 0.92, 0.016, 1);
	}
}